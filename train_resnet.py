import torch
import torch.optim as optim
import torch.nn as nn
import torch.nn.functional as F

import matplotlib

matplotlib.use("Agg")

from torchtrain import TrainingBatch, Settings, HParams

from metrics import (
    LossMetric,
    ClassificationMetric,
    GlobalClassificationMetric,
    MAPMetric,
)
from models import ResNetTranscription, ConvNet
from dataset import MaestroDataset
from loss import BLNLoss

import os


class TrainResNet(TrainingBatch):
    __scope__ = "training.resnet"

    def __init__(self, **kwargs):
        super(TrainResNet, self).__init__(**kwargs)

        self.metrics.add_metrics(
            ["train", "validate", "test"], LossMetric(device=self.device)
        )
        self.metrics.add_metrics(
            ["validate", "test"],
            ClassificationMetric(num_class=88, device=self.device),
        )
        self.metrics.add_metrics(
            ["validate", "test"], GlobalClassificationMetric(device=self.device),
        )
        self.metrics.add_metrics(
            ["validate", "test"], MAPMetric(num_class=88, device=self.device),
        )

    def learning_rate_scheduler(self, optimizer, global_step):
        if global_step >= self.phase3_step:
            lr = self.phase3_lr
        elif global_step >= self.phase2_step:
            lr = self.phase2_lr
        else:
            lr = self.phase1_lr

        optimizer.param_groups[0]["lr"] = lr

    # init or load dataset
    def init(
        self,
        hparams: Settings,
        dataset_path: str,
        train: bool = True,
        validation: bool = True,
        test: bool = True,
    ):
        with hparams.scope(self.__scope__) as p:
            self.batch_size = p.batch_size
            self.epoch = p.epoch

            if p.type == "resnet":
                with hparams.scope("model.resnet") as p:
                    self.model = ResNetTranscription(**p.params)
            elif p.type == "convnet":
                with hparams.scope("model.conv_net") as p:
                    self.model = ConvNet(**p.params)
            else:
                raise Exception(f"Error: {p.type} is an unknown type")

        self.model = self.model.to(self.device)

        with hparams.scope(self.__scope__) as p:
            self.bln = p.bln.enable
            if self.bln:
                self.loss = BLNLoss(eta=p.bln.eta, k=p.bln.k, multiclass=True)
            else:
                self.loss = nn.BCELoss()

            self.phase1_lr = p.optim.phase1.lr
            self.phase2_step = p.optim.phase2.step
            self.phase2_lr = p.optim.phase2.lr
            self.phase3_step = p.optim.phase3.step
            self.phase3_lr = p.optim.phase2.lr

            if p.optim.name == "adam":
                self.optimizer = [
                    optim.Adam(self.model.parameters(), lr=self.phase1_lr)
                ]
            elif p.optim.name == "sgd":
                self.optimizer = [optim.SGD(self.model.parameters(), lr=self.phase1_lr)]
            else:
                raise Exception(f"Optimizer named {p.optim.name} is unkown")

        # setup data loader
        with hparams.scope(self.__scope__) as p:
            loader = MaestroDataset(
                dataset_path,
                batch_size=self.batch_size,
                num_workers=self.num_workers,
                vocabulary=False,
            )
            self.train_loader = loader.train
            self.validation_loader = loader.validation
            self.test_loader = loader.test
            self.step_per_epoch = len(self.train_loader)

    # train on one batch
    def train_batch(self, data, model, optimizer, loss, batch):
        optimizer[0].zero_grad()
        self.learning_rate_scheduler(optimizer[0], batch)

        spec = data.spec.to(self.device)
        frames = (data.frames.to(self.device) != 0).float()
        # onsets = data.onsets.to(self.device)
        # offsets = data.offsets.to(self.device)
        # weigths = data.weigths.to(self.device)

        pred = model(spec)
        if self.bln:
            loss, _, _ = self.loss(pred.view(-1, 88), frames.view(-1, 88))
        else:
            loss = self.loss(pred.view(-1, 88), frames.view(-1, 88))

        loss.backward()
        optimizer[0].step()

        res = {"loss": loss, "prediction": pred, "ground_truth": frames}

        return res

    # validate on one batch
    def validate_batch(self, data, model, optimizer, loss, batch):
        spec = data.spec.to(self.device)
        frames = (data.frames.to(self.device) != 0).float()
        # onsets = data.onsets.to(self.device)
        # offsets = data.offsets.to(self.device)
        # weigths = data.weigths.to(self.device)

        pred = model(spec)
        if self.bln:
            loss, _, _ = self.loss(pred.view(-1, 88), frames.view(-1, 88))
        else:
            loss = self.loss(pred.view(-1, 88), frames.view(-1, 88))

        res = {"loss": loss, "prediction": pred, "ground_truth": frames}

        return res

    # test on one batch
    def test_batch(self, data, model, optimizer, loss, batch):
        return self.validate_batch(data, model, optimizer, loss, batch)


with Settings.default.scope(TrainResNet.__scope__) as hparams:
    hparams.batch_size = 8

    hparams.type = "resnet"

    hparams.optim.name = "adam"
    hparams.optim.phase1.lr = 0.001
    hparams.optim.phase2.step = 32000
    hparams.optim.phase2.lr = 0.0001
    hparams.optim.phase3.step = 48000
    hparams.optim.phase3.lr = 0.00001

    hparams.bln.enable = True
    hparams.bln.eta = 0.01
    hparams.bln.k = 10
