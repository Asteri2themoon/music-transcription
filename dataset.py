import torch
import torch.nn.functional as F
from torch.utils.data import Dataset, DataLoader, SubsetRandomSampler

import tensorflow as tf

tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)

import magenta.music as mm
from magenta.music import sequences_lib
from magenta.music.protobuf import music_pb2
from magenta.music.musicnet_io import note_interval_tree_to_sequence_proto

import librosa
import scipy.io.wavfile as wavfile

import numpy as np

from torchtrain import Settings, HParams, tprint

from utils import labels2vocabulary

import time
import os
import sys
import glob
import json
import copy
import wave


class MaestroBatch:
    def __init__(self, data):
        data = list(zip(*data))

        self.spec = torch.stack(data[0], 0)
        self.frames = torch.stack(data[1], 0)
        self.onsets = torch.stack(data[2], 0)
        self.offsets = torch.stack(data[3], 0)
        self.weigths = torch.stack(data[4], 0)
        if data[5][0] is None:
            self.normalized = None
        else:
            self.normalized = torch.stack(data[5], 0)
        if data[6][0] is None:
            self.vocabulary = None
        else:
            self.vocabulary = torch.stack(data[6], 0)
        if data[7][0] is None:
            self.length = None
        else:
            self.length = torch.stack(data[7], 0)

    # custom memory pinning method on custom type
    def pin_memory(self):
        self.spec = self.spec.pin_memory()
        self.frames = self.frames.pin_memory()
        self.onsets = self.onsets.pin_memory()
        self.offsets = self.offsets.pin_memory()
        self.weigths = self.weigths.pin_memory()
        if self.normalized is not None:
            self.normalized = self.normalized.pin_memory()
        if self.vocabulary is not None:
            self.vocabulary = self.vocabulary.pin_memory()

        return self

    @staticmethod
    def collate_wrapper(batch):
        return MaestroBatch(batch)


class MaestroSubset(Dataset):
    __scope__ = "dataset.maestro"

    def scope(self):
        return self.hparams.scope(self.__scope__, expand=False)

    def __init__(
        self,
        subset,
        hparams,
        augmentation: bool = False,
        max=None,
        freq: torch.Tensor = None,
    ):
        self.hparams = hparams
        self.subset = subset
        self.augmentation = augmentation
        self.max = max

        with self.hparams.scope("dataset.maestro") as p:
            self.midi_offset = p.midi.pitch.min
            self.timestep = p.spec.hop_length / p.spec.sample_rate
            self.vocabulary = p.vocabulary
            self.seq_length = int(round(p.sequence.length / self.timestep))

            self.gain_max = p.augmentation.gain_max
            self.crop_freq_max_count = p.augmentation.crop_freq_max_count
            self.crop_freq_max_size = p.augmentation.crop_freq_max_size
            self.crop_time_max_count = p.augmentation.crop_time_max_count
            self.crop_time_max_size = p.augmentation.crop_time_max_size

        if freq is None:
            self.freq = None
        else:
            self.freq = freq

    def set_frequency(self, freq: torch.Tensor):
        self.freq = freq

    def __len__(self):
        if self.max is not None:
            return min(self.max, len(self.subset))
        else:
            return len(self.subset)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.item()

        audio_path, midi_path, i_seq = self.subset[idx]

        with self.scope() as p:

            numpy_preprocess = midi_path + ".npy"

            data = np.load(numpy_preprocess)

            data = MaestroSubset.cut_pianoroll(
                data,
                hparams=p,
                begin=i_seq * p.sequence.length,
                length=p.sequence.length,
            )
            wav = MaestroSubset.load_wav(
                audio_path,
                hparams=p,
                begin=i_seq * p.sequence.length,
                length=p.sequence.length,
            )
            spec = MaestroSubset.wav_to_spec(wav, p)

            k = p.spec.sample_rate / p.spec.hop_length
            i_length = int(p.sequence.length * k)

            p = i_length - spec.shape[-1]
            if p > 0:
                padding = np.zeros((spec.shape[0], p), dtype=spec.dtype)
                spec = np.concatenate([spec, padding], axis=1)

        length = min(data.shape[-1], spec.shape[-1])

        normalized = None
        if self.freq is not None:
            normalized = (torch.from_numpy(data[0, :, :length]).t_() != 0).float()
            std = torch.sqrt(self.freq * (1.0 - self.freq))
            normalized = (normalized - self.freq) / std

        if self.vocabulary:
            seq = labels2vocabulary(
                torch.from_numpy(data[1, :, :length]).t_(),
                torch.from_numpy(data[2, :, :length]).t_(),
                midi_offset=self.midi_offset,
                timestep=self.timestep,
            )
            if seq.shape[0] < self.seq_length:
                seq_length = torch.tensor(seq.shape[0])
                seq = F.pad(seq, (0, self.seq_length - seq.shape[0])).long()
            elif seq.shape[0] > self.seq_length:
                seq_length = torch.tensor(self.seq_length)
                seq = seq[: self.seq_length].long()
                # raise Exception(f"seq is too long {seq.shape[0]}")
            else:
                seq_length = torch.tensor(self.seq_length)
                seq = seq.long()
        else:
            seq = None
            seq_length = None

        spec_aug = torch.from_numpy(spec[:, :length]).t_()
        if self.augmentation:
            spec_aug = self.random_transform_(spec_aug)

        return (
            spec_aug,
            torch.from_numpy(data[0, :, :length]).t_(),
            torch.from_numpy(data[1, :, :length]).t_(),
            torch.from_numpy(data[2, :, :length]).t_(),
            torch.from_numpy(data[3, :, :length]).t_(),
            normalized,
            seq,
            seq_length,
        )

    def random_transform_(self, sample):
        # random gain
        random_gain = (2 * torch.rand(1) - 1) * self.gain_max
        sample += random_gain

        # crop frequency
        if self.crop_freq_max_count > 0:
            crop_count = torch.randint(1, self.crop_freq_max_count, (1,))
            for _ in range(crop_count):
                freq_range = torch.randint(1, self.crop_freq_max_size, (1,))
                max_freq = sample.shape[-1] - freq_range
                freq = torch.randint(0, max_freq.item(), (1,))

                sample[:, freq : freq + freq_range] = 0

        # crop time
        if self.crop_time_max_count > 0:
            crop_count = torch.randint(1, self.crop_time_max_count, (1,))
            for _ in range(crop_count):
                time_range = torch.randint(1, self.crop_time_max_size, (1,))
                max_time = sample.shape[-2] - time_range
                time = torch.randint(0, max_time.item(), (1,))

                sample[time : time + time_range, :] = 0

        return sample

    def get_dataloader(
        self,
        batch_size: int,
        num_workers: int = 8,
        shuffle: bool = True,
        pin_memory: bool = True,
    ):
        return DataLoader(
            self,
            batch_size=batch_size,
            num_workers=num_workers,
            collate_fn=MaestroBatch.collate_wrapper,
            shuffle=shuffle,
            pin_memory=pin_memory,
        )

    @staticmethod
    def load_wav(file_name: str, hparams, begin: float, length: float):
        with wave.open(file_name, "r") as audio:
            audio_sampwidth = audio.getsampwidth()
            audio_channel = audio.getnchannels()
            audio_rate = audio.getframerate()
            audio_length = audio.getnframes()

            # calculate index of the first data and the length
            i_begin = np.floor(begin * audio_rate).astype(int).item()
            i_length = np.floor(length * audio_rate).astype(int).item()
            i_length = min(i_length, audio_length - i_begin)

            assert audio_sampwidth == 2

            # go to the begining of the requested part
            audio.setpos(i_begin)

            # read the requested part
            raw_data = np.frombuffer(
                audio.readframes(i_length),
                count=i_length * audio_channel,
                dtype=np.int16,
            )

            # to mono
            if audio_channel > 1:
                wav = raw_data.reshape((i_length, audio_channel)).mean(axis=1)

            # resample if needed
            if hparams.spec.sample_rate != audio_rate:
                wav = librosa.core.resample(wav, audio_rate, hparams.spec.sample_rate)

        return wav

    @staticmethod
    def cut_pianoroll(pianoroll, hparams, begin: float, length: float):
        sample_rate = hparams.spec.sample_rate
        hop_length = hparams.spec.hop_length
        k = sample_rate / hop_length

        # calculate index of the first data and the length
        i_begin = np.floor(begin * k).astype(int).item()
        i_length = np.floor(length * k).astype(int).item()

        if pianoroll.shape[-1] >= i_begin + i_length:
            return pianoroll[:, :, i_begin : i_begin + i_length]
        else:
            p = (i_begin + i_length) - pianoroll.shape[-1]
            padding = np.zeros_like(pianoroll[:, :, -p:])
            return np.concatenate([pianoroll[:, :, i_begin:], padding], axis=2)

    @staticmethod
    def wav_to_cqt(wav, hparams):
        spec = np.abs(
            librosa.core.cqt(
                wav,
                hparams.spec.sample_rate,
                hop_length=hparams.spec.hop_length,
                fmin=hparams.spec.cqt.fmin,
                n_bins=hparams.spec.cqt.n_bins,
                bins_per_octave=hparams.spec.cqt.bins_per_octave,
            )
        ).astype(np.float32)
        if hparams.spec.cqt.log_amp:
            spec = librosa.power_to_db(spec)
        if hparams.spec.normalize:
            spec = librosa.util.normalize(spec)
        return spec

    @staticmethod
    def wav_to_hybrid_cqt(wav, hparams):
        spec = np.abs(
            librosa.core.hybrid_cqt(
                wav,
                hparams.spec.sample_rate,
                hop_length=hparams.spec.hop_length,
                fmin=hparams.spec.cqt.fmin,
                n_bins=hparams.spec.cqt.n_bins,
                bins_per_octave=hparams.spec.cqt.bins_per_octave,
            )
        ).astype(np.float32)
        if hparams.spec.cqt.log_amp:
            spec = librosa.power_to_db(spec)
        if hparams.spec.normalize:
            spec = librosa.util.normalize(spec)
        return spec

    @staticmethod
    def wav_to_mel(wav, hparams):
        spec = librosa.feature.melspectrogram(
            wav,
            hparams.spec.sample_rate,
            hop_length=hparams.spec.hop_length,
            fmin=hparams.spec.mel.fmin,
            n_mels=hparams.spec.mel.n_bins,
            htk=hparams.spec.mel.htk,
        ).astype(np.float32)
        if hparams.spec.mel.log_amp:
            spec = librosa.power_to_db(spec)
        if hparams.spec.normalize:
            spec = librosa.util.normalize(spec)
        return spec

    @staticmethod
    def wav_to_spec(wav, hparams):
        spec = {
            "mel": MaestroSubset.wav_to_mel,
            "cqt": MaestroSubset.wav_to_cqt,
            "hybrid_cqt": MaestroSubset.wav_to_hybrid_cqt,
        }

        return spec[hparams.spec.type](wav, hparams)

    @staticmethod
    def sequence_to_pianoroll(
        sequence, hparams: HParams, begin: float = 0, length: float = -1
    ):
        velocities = [note.velocity for note in sequence.notes]
        velocity_max = np.max(velocities) if velocities else 0
        velocity_min = np.min(velocities) if velocities else 0
        velocity_range = music_pb2.VelocityRange(min=velocity_min, max=velocity_max)
        if hparams.midi.sustain_control:
            sequence = sequences_lib.apply_sustain_control_changes(sequence)

        seq_inst = {}
        for note in sequence.notes:
            if not (note.program in seq_inst):
                seq_inst[note.program] = []
            seq_inst[note.program].append(note)

        frame_pre_sec = hparams.spec.sample_rate / hparams.spec.hop_length
        tmp = music_pb2.NoteSequence()
        tmp.notes.extend(seq_inst[0])  # juste piano
        tmp.total_time = sequence.total_time

        pianoroll = sequences_lib.sequence_to_pianoroll(
            tmp,
            frames_per_second=frame_pre_sec,
            min_pitch=hparams.midi.pitch.min,
            max_pitch=hparams.midi.pitch.max,
            min_frame_occupancy_for_label=0.0,
            onset_mode="length_ms",
            onset_length_ms=hparams.midi.onset.length,
            offset_length_ms=0.0,
            onset_delay_ms=0.0,
            min_velocity=velocity_range.min,
            max_velocity=velocity_range.max,
        )

        # calculate index of the first data and the length
        i_begin = np.floor(begin * frame_pre_sec).astype(int).item()
        if length == -1:
            i_end = -1
        else:
            i_end = i_begin + np.floor(length * frame_pre_sec).astype(int).item()

        assert i_end <= pianoroll.active.shape[0]

        # active,weights,onsets,onset_velocities,active_velocities,offsets,control_changes

        return (
            np.transpose(pianoroll.active_velocities[i_begin:i_end, :]),
            np.transpose(pianoroll.onset_velocities[i_begin:i_end, :]),
            np.transpose(pianoroll.offsets[i_begin:i_end, :]),
            np.transpose(pianoroll.weights[i_begin:i_end, :]),
        )


"""Maestro dataset."""


class MaestroDataset:
    __scope__ = "dataset.maestro"

    def scope(self):
        return self.hparams.scope(self.__scope__, expand=False)

    def __init__(
        self, file_name: str, batch_size: int, num_workers: int = 8, **kwargs,
    ):
        self.hparams = Settings(hparams=HParams(**kwargs))

        with self.scope() as p:
            seq_length = p.sequence.length
            seq_fill = p.sequence.fill
            validation_max = p.validation.max

        with open(file_name, "r") as f:
            maestro_json = json.load(f)

        maestro_base_dir, _ = os.path.split(file_name)

        dataset = {"train": [], "validation": [], "test": []}

        # put task into the queue
        for data in maestro_json:
            split = data["split"]

            assert split in dataset

            audio_path = os.path.join(maestro_base_dir, data["audio_filename"])
            midi_path = os.path.join(maestro_base_dir, data["midi_filename"])
            duration = data["duration"]

            if seq_fill:
                n = np.ceil(duration / seq_length).astype(int).item()
            else:
                n = np.floor(duration / seq_length).astype(int).item()

            for i_seq in range(n):
                dataset[split].append((audio_path, midi_path, i_seq))

        train = MaestroSubset(dataset["train"], self.hparams, augmentation=True)
        self.train = train.get_dataloader(
            batch_size=batch_size, num_workers=num_workers
        )
        validation = MaestroSubset(
            dataset["validation"], self.hparams, augmentation=False, max=validation_max,
        )
        self.validation = validation.get_dataloader(
            batch_size=batch_size, num_workers=num_workers
        )
        test = MaestroSubset(dataset["test"], self.hparams, augmentation=False)
        self.test = test.get_dataloader(batch_size=batch_size, num_workers=num_workers)

        stat_file_name = os.path.join(maestro_base_dir, "stats.json")
        try:
            self.n, self.freq = self.load_stats(stat_file_name)
        except:
            self.n, self.freq = self.calculate_stats(self.train)
            self.save_stats(stat_file_name, self.n, self.freq)

        train.set_frequency(self.freq / self.n)
        validation.set_frequency(self.freq / self.n)
        test.set_frequency(self.freq / self.n)

    def calculate_stats(self, dataloader) -> torch.Tensor:
        with self.scope() as p:
            notes = p.midi.pitch.max - p.midi.pitch.min + 1
        freq = torch.zeros(notes)
        n = 0
        for i, batch in enumerate(dataloader):
            frames = batch.frames
            n += frames.shape[0] * frames.shape[1]
            print(f"calcualte stats: {(i+1)*100/len(dataloader):.2f}%", end="\r")
            freq += (frames != 0).sum(axis=(0, 1))
        return n, freq

    def load_stats(self, file_name: str) -> torch.Tensor:
        with open(file_name, "r") as fp:
            data = json.load(fp)
            n = data["n"]
            freq = torch.tensor(data["freq"])
        return n, freq

    def save_stats(self, file_name: str, n: int, stats: torch.Tensor) -> None:
        with open(file_name, "w") as fp:
            data = {"n": n, "freq": stats.tolist()}
            json.dump(data, fp)


with Settings.default.scope(MaestroDataset.__scope__) as hparams:
    hparams.vocabulary = True
    hparams.sequence.length = 20.0
    hparams.sequence.fill = True
    hparams.spec.sample_rate = 16000
    hparams.spec.type = "mel"
    hparams.spec.hop_length = 512
    hparams.spec.normalize = True
    hparams.spec.cqt.bins_per_octave = 36
    # calculate fmin for cqt spectrogram
    bins_per_tone = hparams.spec.cqt.bins_per_octave / 12
    # from C4 to C0
    C0 = 440.0 * 2 ** (-4)
    # freq bins alignment
    offset = 2 ** (-(bins_per_tone - 1) / (2 * bins_per_tone) / 12)
    hparams.spec.cqt.fmin = C0 * offset
    # calculate n_bins for cqt spectrogram
    # frequency cut
    fc = hparams.spec.sample_rate / 2
    n_bins = (hparams.spec.cqt.bins_per_octave / np.log(2)) * (
        np.log(fc) - np.log(hparams.spec.cqt.fmin)
    )
    hparams.spec.cqt.n_bins = int(n_bins)
    hparams.spec.cqt.log_amp = True
    hparams.spec.mel.n_bins = 229
    hparams.spec.mel.htk = True
    hparams.spec.mel.log_amp = True
    hparams.spec.mel.fmin = 26.0
    hparams.midi.pitch.min = 21
    hparams.midi.pitch.max = 108
    hparams.midi.sustain_control = True
    hparams.midi.onset.length = 0.032
    hparams.validation.max = 128

    hparams.augmentation.gain_max = 0.1
    hparams.augmentation.crop_freq_max_count = 2
    hparams.augmentation.crop_freq_max_size = 50
    hparams.augmentation.crop_time_max_count = 10
    hparams.augmentation.crop_time_max_size = 30


def preprocess(src: str, dst: str, hparams: Settings, workers: int):
    import librosa
    import scipy.io.wavfile as wavfile
    import numpy as np

    import queue
    import threading
    import os
    import glob
    import time
    import shutil

    with hparams.scope(MaestroDataset.__scope__) as p:
        targeted_rate = p.spec.sample_rate

    print(f"loading {src}")
    with open(src, "r") as f:
        maestro_json = json.load(f)

    maestro_base_dir, maestro_json_file = os.path.split(src)

    try:
        os.makedirs(dst)
    except:
        pass
    new_maestro_json_file = os.path.join(dst, maestro_json_file)
    shutil.copy(src, new_maestro_json_file)

    print(f"preprocessing")

    def preprocess_sample(sample):
        audio_path, midi_path = sample

        input_audio_path = os.path.join(maestro_base_dir, audio_path)
        input_midi_path = os.path.join(maestro_base_dir, midi_path)
        output_audio_path = os.path.join(dst, audio_path)
        output_midi_path = os.path.join(dst, midi_path)
        output_pianoroll_path = os.path.join(dst, f"{midi_path}.npy")

        dst_dir = os.path.split(output_audio_path)[0]
        try:
            os.makedirs(dst_dir)
        except:
            pass

        rate, audio = wavfile.read(input_audio_path)
        audio = np.transpose(audio).astype(float)

        new_audio = librosa.core.resample(audio, rate, targeted_rate).astype(np.int16)

        wavfile.write(
            output_audio_path, targeted_rate, np.transpose(new_audio),
        )

        with hparams.scope(MaestroDataset.__scope__) as p:
            seq = mm.midi_file_to_note_sequence(input_midi_path)
            (
                active_velocities,
                onset_velocities,
                offset,
                weights,
            ) = MaestroSubset.sequence_to_pianoroll(seq, hparams=p, begin=0, length=-1)

            new_shape = (1, *active_velocities.shape)
            concat = np.concatenate(
                [
                    active_velocities.reshape(new_shape),
                    onset_velocities.reshape(new_shape),
                    offset.reshape(new_shape),
                    weights.reshape(new_shape),
                ],
                axis=0,
            )
            np.save(output_pianoroll_path, concat)

        shutil.copy(input_midi_path, output_midi_path)
        del (
            audio,
            new_audio,
            concat,
            active_velocities,
            onset_velocities,
            weights,
        )

    q = queue.Queue()

    def worker():
        while True:
            job = q.get()
            if job is None:
                break
            preprocess_sample(job)

    # put task into the queue
    for data in maestro_json:
        audio_path = data["audio_filename"]
        midi_path = data["midi_filename"]
        q.put((audio_path, midi_path))

    # put stop info
    for _ in range(workers):
        q.put(None)

    # start workers
    threads = []
    for _ in range(workers):
        t = threading.Thread(target=worker)
        t.start()
        threads.append(t)

    # monitor queue
    N = q.qsize()
    while not q.empty():
        tprint.print_line(tprint.bar((N - q.qsize()) / N, 40))
        sys.stdout.flush()
        time.sleep(0.1)

    # stop workers
    for t in threads:
        t.join()


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("--source", "-s", help="json input file")
    parser.add_argument("--destination", "-d", help="output directory")
    parser.add_argument("--workers", "-w", default=8, type=int, help="number of thread")

    args = parser.parse_args()

    preprocess(
        src=args.source, dst=args.destination, hparams=Settings(), workers=args.workers,
    )
