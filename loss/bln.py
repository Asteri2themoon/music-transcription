import torch
import torch.nn.functional as F
import torch.nn as nn


def attributes2minotiry(a, rho=0.5):
    freq = a.sum(axis=0)
    n_bs = freq.sum()

    (freq_nz,) = freq.nonzero(as_tuple=True)
    freq_j = freq_nz[torch.argsort(freq[freq_nz])]

    threshold = int(n_bs * rho)

    minority = freq_j[torch.cumsum(freq[freq_j], dim=0) <= threshold]

    return minority


def get_hard_sample(a, p, k):
    anchor_len = a.sum(axis=0).max().int()
    anchor, anchor_idx = a.sort(axis=0, descending=True)

    anchor_mask = anchor[:anchor_len, :].bool()

    anchor_p = p.gather(dim=0, index=anchor_idx)[:anchor_len, :]

    pos_mask = torch.masked_fill(p, a == 0.0, 2.0)
    neg_mask = torch.masked_fill(p, a == 1.0, -1.0)

    hard_pos_p = torch.topk(pos_mask, k, dim=0, largest=False)[0]
    hard_neg_p = torch.topk(neg_mask, k, dim=0, largest=True)[0]

    hard_pos_mask = hard_pos_p != 2.0
    hard_neg_mask = hard_neg_p != -1.0

    return (
        anchor_p,
        anchor_mask,
        hard_pos_p,
        hard_pos_mask,
        hard_neg_p,
        hard_neg_mask,
    )


def imbalance(a):
    freq = torch.sum(a, axis=0).float()
    (idx,) = torch.nonzero(freq, as_tuple=True)
    freq[idx] = freq.max() / freq[idx] - 1
    return freq


def loss_crl(p, a, weight, minority=None, k=10, rho=0.5, m_j=0.5):
    if minority is None:
        minority = attributes2minotiry(a, rho=rho)

    a_m = a[:, minority]
    p_m = p[:, minority]
    w_m = weight[minority]

    (
        anchor_p,
        anchor_mask,
        hard_pos_p,
        hard_pos_mask,
        hard_neg_p,
        hard_neg_mask,
    ) = get_hard_sample(a_m, p_m, k)

    a_idx = torch.arange(0, anchor_p.shape[0], dtype=int)
    c_idx = torch.arange(0, anchor_p.shape[1], dtype=int)
    h_idx = torch.arange(0, k, dtype=int)

    a, j, h = torch.meshgrid(a_idx, c_idx, h_idx)
    d_pos_mask = anchor_mask[a, j] & hard_pos_mask[h, j]
    d_neg_mask = anchor_mask[a, j] & hard_neg_mask[h, j]
    d_pos = torch.abs(anchor_p[a, j] - hard_pos_p[h, j]) * d_pos_mask
    d_neg = (anchor_p[a, j] - hard_neg_p[h, j]) * d_neg_mask

    j, a, p, n = torch.meshgrid(c_idx, a_idx, h_idx, h_idx)
    triplet_mask = d_pos_mask[a, j, p] & d_neg_mask[a, j, n]
    triplet = F.relu((d_pos[a, j, p] - d_neg[a, j, n] + m_j) * triplet_mask)

    # print(anchor_p.shape)
    triplet_byclass = triplet.view(anchor_p.shape[1], -1).sum(axis=1)
    N_byclass = triplet_mask.view(anchor_p.shape[1], -1).sum(axis=1).float()

    (nz_idx,) = torch.nonzero(N_byclass, as_tuple=True)
    # print(N_byclass)
    loss = (triplet_byclass[nz_idx] / N_byclass[nz_idx] * w_m[nz_idx]).mean()
    # print(loss)
    # input("enter to continiue")
    return loss


def loss_bln(
    p,
    a,
    eta,
    omega=None,
    minority=None,
    k=10,
    rho=0.5,
    m_j=0.5,
    multiclass=False,
    custom_loss=None,
):
    if a.dtype == torch.long:
        a_vector = F.one_hot(a, num_classes=p.shape[-1])
    else:
        a_vector = a

    if omega is None:
        omega = imbalance(a_vector)
    alpha = torch.clamp(eta * omega, min=0.0, max=1.0).detach().clone()

    l_crl = loss_crl(
        p, a_vector, weight=alpha, minority=minority, k=k, rho=rho, m_j=m_j
    )

    if custom_loss is not None:
        l_ce = custom_loss(p, a, weight=(1 - alpha))
    elif multiclass:
        l_ce = F.binary_cross_entropy(p, a, weight=(1 - alpha))
    else:
        l_ce = F.cross_entropy(p, a, weight=(1 - alpha))

    l_bln = l_crl + l_ce

    return l_bln, l_ce, l_crl


class BLNLoss(nn.Module):
    def __init__(
        self, eta=0.01, k=10, rho=0.5, m_j=0.5, multiclass=False, custom_loss=None
    ):
        super(BLNLoss, self).__init__()

        self.eta = eta
        self.k = k
        self.rho = rho
        self.m_j = m_j
        self.multiclass = multiclass
        self.custom_loss = custom_loss

    def forward(self, prediction, target, omega=None, minority=None):
        return loss_bln(
            prediction,
            target,
            eta=self.eta,
            omega=omega,
            minority=minority,
            k=self.k,
            rho=self.rho,
            m_j=self.m_j,
            multiclass=self.multiclass,
            custom_loss=self.custom_loss,
        )
