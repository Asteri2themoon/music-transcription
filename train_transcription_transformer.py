import torch
import torch.optim as optim
import torch.nn as nn
import torch.nn.functional as F
from torch.multiprocessing import Pool

import matplotlib

matplotlib.use("Agg")

from torchtrain import TrainingBatch, Settings, HParams

from metrics import LossMetric, MIREvalMetric, TopKMetric
from models import TranscriptionTransformer, ResNetTranscription, ConvNet
from dataset import MaestroDataset
from utils import (
    vocabulary2mir_eval,
    labels2mir_eval,
    frames2mir_eval,
    frames2onsets_offsets,
    pool_labels2mir_eval,
)
from loss import BLNLoss

import math
import os


class TrainTranscriptionTransformer(TrainingBatch):
    __scope__ = "training.transcriptiontransformer"

    def __init__(self, **kwargs):
        super(TrainTranscriptionTransformer, self).__init__(**kwargs)

        with self.hparams.scope(self.__scope__) as p:
            self.k = p.k
        with self.hparams.scope(TranscriptionTransformer.__scope__) as p:
            self.notes = p.trans.output_size

        self.metrics.add_metrics(
            ["train", "validate", "test"], LossMetric(device=self.device)
        )

        with self.hparams.scope(MaestroDataset.__scope__) as p:
            timestep = p.spec.hop_length / p.spec.sample_rate

        self.metrics.add_metrics(
            ["validate", "test"], MIREvalMetric(timestep=timestep, device=self.device),
        )

    def learning_rate_scheduler_rsqrt(self, optimizer, global_step):
        warmup = min(global_step / self.warmup_step, 1.0)
        decay = 1 / math.sqrt(max(global_step, self.warmup_step))

        optimizer.param_groups[0]["lr"] = self.lr * self.rsqtr_hidden * warmup * decay

    def tsa_keep(self, global_step, pred, labels):
        if self.tsa == "none":
            return None

        pred_softmax = F.softmax(pred, dim=-1)

        if self.tsa_afterwarmup:
            if global_step < self.warmup_step:
                return None
            ratio = (global_step - self.warmup_step) / (
                self.iter_train_total - self.warmup_step
            )
        else:
            ratio = global_step / self.iter_train_total

        if self.tsa == "linear":
            alpha_t = torch.tensor(ratio)
        elif self.tsa == "exp":
            alpha_t = torch.exp(torch.tensor((ratio - 1) * 5))
        elif self.tsa == "log":
            alpha_t = 1 - torch.exp(torch.tensor(-ratio * 5))
        else:
            raise Exception(f"TSA type {self.tsa} unknown")

        K = self.output_dim
        threshold = (alpha_t * (1.0 - 1.0 / K)) + 1.0 / K

        values, args = torch.max(pred_softmax, dim=-1)
        low_confidence = (values < threshold) | (args != labels)

        return low_confidence

    # init or load dataset
    def init(
        self,
        hparams: Settings,
        dataset_path: str,
        train: bool = True,
        validation: bool = True,
        test: bool = True,
    ):
        with hparams.scope(self.__scope__) as p:
            self.batch_size = p.batch_size

            if len(p.embedding.checkpoint) > 0:
                checkpoint = torch.load(p.embedding.checkpoint)
                if (
                    checkpoint["model"]["hparams"]["training"]["resnet"]["type"]
                    == "resnet"
                ):
                    architecture = checkpoint["hparams"]["model"]["resnet"]["conv_net"]
                    # print(architecture)
                    self.embedding = ResNetTranscription(
                        embedding=True, conv_net=architecture
                    )
                elif (
                    checkpoint["model"]["hparams"]["training"]["resnet"]["type"]
                    == "convnet"
                ):
                    self.embedding = ConvNet(embedding=True)
                else:
                    raise Exception(
                        f"model based on unkown class {checkpoint['model']['class']}"
                    )
                self.embedding.from_dict(
                    checkpoint["model"], device=self.device, strict=False
                )
            else:
                self.embedding = None

        with hparams.scope(TranscriptionTransformer.__scope__) as p:
            self.output_dim = p.output.dim
            self.model = TranscriptionTransformer(**p.params)
            self.model.eval()
        self.model = self.model.to(self.device)

        with hparams.scope(self.__scope__) as p:
            assert (p.vocabulary and (not p.normalized)) or (not p.vocabulary)
            assert (p.bln.enable and (not p.normalized)) or (not p.bln.enable)
            assert ((not p.vocabulary) and (p.tsa.type == "none")) or p.vocabulary

            self.vocabulary = p.vocabulary
            self.normalized = p.normalized

            self.warmup_step = p.warmup_step
            self.lr = p.lr
            self.beta1 = p.beta1
            self.beta2 = p.beta2
            self.clip_grad = p.clip_grad
            self.tsa = p.tsa.type
            self.tsa_afterwarmup = p.tsa.afterwarmup

            self.rsqtr_hidden = 1 / math.sqrt(self.model.d_model)

            self.optimizer = [
                optim.Adam(self.model.parameters(), betas=(self.beta1, self.beta2))
            ]

            self.bln = p.bln.enable
            if self.bln:
                custom_loss = None
                self.loss = BLNLoss(
                    eta=p.bln.eta, k=p.bln.k, multiclass=not self.vocabulary,
                )
            elif self.vocabulary:
                self.loss = nn.CrossEntropyLoss()
            elif self.normalized:
                self.loss = nn.MSELoss()
            else:
                self.loss = nn.BCELoss()

        if self.vocabulary:
            self.metrics.add_metrics(
                ["validate", "test"], TopKMetric(device=self.device),
            )

        # setup data loader
        with hparams.scope(self.__scope__) as p:
            loader = MaestroDataset(
                dataset_path,
                batch_size=self.batch_size,
                num_workers=self.num_workers,
                vocabulary=self.vocabulary,
            )
            self.train_loader = loader.train
            self.validation_loader = loader.validation
            self.test_loader = loader.test

    # train on one batch
    def train_batch(self, data, model, optimizer, loss, batch):
        optimizer[0].zero_grad()

        spec = data.spec.to(self.device)
        if self.normalized:
            normalized = data.normalized.to(self.device)
        else:
            normalized = None

        if self.vocabulary:
            vocabulary = data.vocabulary.to(self.device)
            one_hot_vector = torch.zeros(
                (*vocabulary.shape, self.output_dim), device=self.device
            )
            b, t = torch.meshgrid(
                [
                    torch.arange(0, vocabulary.shape[0], dtype=torch.long),
                    torch.arange(0, vocabulary.shape[1], dtype=torch.long),
                ]
            )
            one_hot_vector[b, t, vocabulary[b, t]] = 1.0
        else:
            frames = data.frames.to(self.device)

        if self.embedding is not None:
            with torch.no_grad():
                spec_embedding = self.embedding(spec)
        else:
            spec_embedding = spec

        self.learning_rate_scheduler_rsqrt(optimizer[0], batch)

        if self.vocabulary:
            padded_vector = F.pad(one_hot_vector[:, :-1], (0, 0, 1, 0))
            pred = model(spec_embedding, padded_vector)
            pred_lst, seq_lst = [], []
            for p, s, l in zip(pred, vocabulary, data.length):
                if l > 1:
                    pred_lst.append(p[:l])
                    seq_lst.append(s[:l])
            pred = torch.cat(tuple(pred_lst), 0)
            seq = torch.cat(tuple(seq_lst), 0)

            keep = self.tsa_keep(batch, pred, seq)

            if keep is not None:
                loss = self.loss(pred[keep], seq[keep])
            else:
                loss = self.loss(pred, seq)
            # = F.cross_entropy(pred, seq)
        else:
            pred = model(spec_embedding, frames)

            if not self.normalized:
                pred = torch.sigmoid(pred)[:, :-1].reshape(-1, pred.shape[-1])
                target = frames[:, 1:].reshape(-1, pred.shape[-1])
            else:
                pred = pred[:, 1:].reshape(-1, pred.shape[-1])
                target = normalized[:, 1:].reshape(-1, pred.shape[-1])

            loss = self.loss(pred, target)
            # loss = F.binary_cross_entropy(
            #    pred[:, :-1].reshape(-1, pred.shape[-1]),
            #    frames[:, 1:].reshape(-1, pred.shape[-1]),
            # )

        if type(loss) == tuple:
            loss = loss[0]

        loss.backward()

        torch.nn.utils.clip_grad_norm_(model.parameters(), self.clip_grad)
        optimizer[0].step()

        res = {"loss": loss}

        return res

    # validate on one batch
    def validate_batch(self, data, model, optimizer, loss, batch):
        spec = data.spec.to(self.device)

        if self.normalized:
            normalized = data.normalized.to(self.device)
        else:
            normalized = None

        if self.vocabulary:
            vocabulary = data.vocabulary.to(self.device)
            one_hot_vector = torch.zeros(
                (*vocabulary.shape, self.output_dim), device=self.device
            )
            b, t = torch.meshgrid(
                [
                    torch.arange(0, vocabulary.shape[0], dtype=torch.long),
                    torch.arange(0, vocabulary.shape[1], dtype=torch.long),
                ]
            )
            one_hot_vector[b, t, vocabulary[b, t]] = 1.0
        else:
            frames = data.frames.to(self.device)

        if self.embedding is not None:
            spec_embedding = self.embedding(spec)
        else:
            spec_embedding = spec

        self.learning_rate_scheduler_rsqrt(optimizer[0], batch)

        if self.vocabulary:
            # teacher forcing
            padded_vector = F.pad(one_hot_vector[:, :-1], (0, 0, 1, 0))
            raw_est = model(spec_embedding, padded_vector)
            pred_lst, seq_lst = [], []
            for p, s, l in zip(raw_est, vocabulary, data.length):
                # if l >= 1:
                pred_lst.append(p[:l])
                seq_lst.append(s[:l])
            pred = torch.cat(tuple(pred_lst), 0)
            seq = torch.cat(tuple(seq_lst), 0)

            keep = None  # self.tsa_keep(batch, pred, seq)

            if keep is not None:
                loss = self.loss(pred[keep], seq[keep])
            else:
                loss = self.loss(pred, seq)

            loss = self.loss(pred, seq)
            # = F.cross_entropy(pred, seq)

            # gready decoding
            pred = model(spec_embedding)

            ref, est = [], []
            for seq in vocabulary:
                ref.append(vocabulary2mir_eval(seq))
            for seq in torch.argmax(pred, dim=-1):
                est.append(vocabulary2mir_eval(seq))
        else:
            # teacher forcing
            pred = model(spec_embedding, frames)

            if not self.normalized:
                pred = torch.sigmoid(pred)[:, :-1].reshape(-1, pred.shape[-1])
                target = frames[:, 1:].reshape(-1, pred.shape[-1])
            else:
                pred = pred[:, 1:].reshape(-1, pred.shape[-1])
                target = normalized[:, 1:].reshape(-1, pred.shape[-1])

            loss = self.loss(pred, target)
            # loss = F.binary_cross_entropy(
            #    pred[:, :-1].reshape(-1, pred.shape[-1]),
            #    frames[:, 1:].reshape(-1, pred.shape[-1]),
            # )

            # gready decoding
            pred = model(spec_embedding)
            ref, est = [], []
            for (onsets, offsets) in zip(data.onsets, data.offsets):
                ref.append(labels2mir_eval(onsets, offsets))

            onsets, offsets = frames2onsets_offsets(pred)

            with Pool(pred.shape[0]) as p:
                est = p.map(
                    pool_labels2mir_eval, list(zip(onsets.cpu(), offsets.cpu())),
                )
            pred_lst, seq_lst = None, None

        if type(loss) == tuple:
            loss = loss[0]

        res = {
            "loss": loss,
            "est": est,
            "raw_est": pred_lst,
            "seq": seq_lst,
            "ref": ref,
        }

        return res

    # test on one batch
    def test_batch(self, data, model, optimizer, loss, batch):
        return self.validate_batch(data, model, optimizer, loss, batch)


with Settings.default.scope(TrainTranscriptionTransformer.__scope__) as hparams:
    hparams.batch_size = 8

    hparams.bln.enable = False
    hparams.bln.eta = 0.01
    hparams.bln.k = 10

    hparams.tsa.type = "exp"
    hparams.tsa.afterwarmup = True

    hparams.warmup_step = 16000
    hparams.lr = 1.0
    hparams.beta1 = 0.9
    hparams.beta2 = 0.98
    hparams.clip_grad = 1.0

    hparams.vocabulary = True
    hparams.normalized = False
    hparams.k = 5

    hparams.embedding.checkpoint = "checkpoint/embedding.pth"
