from .onset_and_frame import OnsetAndFrame
from .transformer import TranscriptionTransformer
from .resnet import ResNetTranscription
from .convnet import ConvNet
