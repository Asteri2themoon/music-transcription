import torch
import torch.nn as nn
import torch.nn.functional as F

import torchvision.models as models

from torchtrain import DeepModel, Settings

from .tools import AvgPoolingFreq, LinearSeq, Identity


class ResNetTranscription(DeepModel):
    __scope__ = "model.resnet"
    output_size = 88

    def __init__(self, embedding: bool = False, **kwargs):
        super(ResNetTranscription, self).__init__(**kwargs)

        with self.scope() as p:
            self.conv_net = p.conv_net

            print(self.conv_net)
            if self.conv_net == "resnet18":
                self.cnn = self._build_resnet18(embedding=embedding)
            elif self.conv_net == "resnet34":
                self.cnn = self._build_resnet34(embedding=embedding)
            elif self.conv_net == "resnet50":
                self.cnn = self._build_resnet50(embedding=embedding)
            else:
                raise Exception(f"unkown embedding type ({self.conv_net})")

    @staticmethod
    def _build_resnet18(embedding: bool = False):
        resnet = models.resnet18()
        resnet.conv1 = nn.Conv2d(
            1, 64, kernel_size=(7, 7), stride=(1, 2), padding=(3, 3), bias=False
        )
        resnet.maxpool = nn.MaxPool2d(
            kernel_size=3, stride=(1, 2), padding=1, dilation=1, ceil_mode=False
        )
        resnet.layer2[0].conv1 = nn.Conv2d(
            64, 128, kernel_size=(3, 3), stride=(1, 2), padding=(1, 1), bias=False,
        )
        resnet.layer2[0].downsample[0] = nn.Conv2d(
            64, 128, kernel_size=(1, 1), stride=(1, 2), bias=False
        )
        resnet.layer3[0].conv1 = nn.Conv2d(
            128, 256, kernel_size=(3, 3), stride=(1, 2), padding=(1, 1), bias=False,
        )
        resnet.layer3[0].downsample[0] = nn.Conv2d(
            128, 256, kernel_size=(1, 1), stride=(1, 2), bias=False
        )
        resnet.layer4[0].conv1 = nn.Conv2d(
            256, 512, kernel_size=(3, 3), stride=(1, 2), padding=(1, 1), bias=False,
        )
        resnet.layer4[0].downsample[0] = nn.Conv2d(
            256, 512, kernel_size=(1, 1), stride=(1, 2), bias=False,
        )
        resnet.avgpool = AvgPoolingFreq()
        if embedding:
            resnet.fc = Identity(512)
        else:
            resnet.fc = LinearSeq(512, ResNetTranscription.output_size)

        return resnet

    @staticmethod
    def _build_resnet34(embedding: bool = False):
        resnet = models.resnet34()
        resnet.conv1 = nn.Conv2d(
            1, 64, kernel_size=(7, 7), stride=(1, 2), padding=(3, 3), bias=False
        )
        resnet.maxpool = nn.MaxPool2d(
            kernel_size=3, stride=(1, 2), padding=1, dilation=1, ceil_mode=False
        )
        resnet.layer2[0].conv1 = nn.Conv2d(
            64, 128, kernel_size=(3, 3), stride=(1, 2), padding=(1, 1), bias=False,
        )
        resnet.layer2[0].downsample[0] = nn.Conv2d(
            64, 128, kernel_size=(1, 1), stride=(1, 2), bias=False
        )
        resnet.layer3[0].conv1 = nn.Conv2d(
            128, 256, kernel_size=(3, 3), stride=(1, 2), padding=(1, 1), bias=False,
        )
        resnet.layer3[0].downsample[0] = nn.Conv2d(
            128, 256, kernel_size=(1, 1), stride=(1, 2), bias=False
        )
        resnet.layer4[0].conv1 = nn.Conv2d(
            256, 512, kernel_size=(3, 3), stride=(1, 2), padding=(1, 1), bias=False,
        )
        resnet.layer4[0].downsample[0] = nn.Conv2d(
            256, 512, kernel_size=(1, 1), stride=(1, 2), bias=False,
        )
        resnet.avgpool = AvgPoolingFreq()
        if embedding:
            resnet.fc = Identity(512)
        else:
            resnet.fc = LinearSeq(512, ResNetTranscription.output_size)

        return resnet

    @staticmethod
    def _build_resnet50(embedding: bool = False):
        resnet = models.resnet50()
        resnet.conv1 = nn.Conv2d(
            1, 64, kernel_size=(7, 7), stride=(1, 2), padding=(3, 3), bias=False
        )
        resnet.maxpool = nn.MaxPool2d(
            kernel_size=3, stride=(1, 2), padding=1, dilation=1, ceil_mode=False
        )
        resnet.layer2[0].conv2 = nn.Conv2d(
            128, 128, kernel_size=(3, 3), stride=(1, 2), padding=(1, 1), bias=False,
        )
        resnet.layer2[0].downsample[0] = nn.Conv2d(
            256, 512, kernel_size=(1, 1), stride=(1, 2), bias=False
        )
        resnet.layer3[0].conv2 = nn.Conv2d(
            256, 256, kernel_size=(3, 3), stride=(1, 2), padding=(1, 1), bias=False,
        )
        resnet.layer3[0].downsample[0] = nn.Conv2d(
            512, 1024, kernel_size=(1, 1), stride=(1, 2), bias=False
        )
        resnet.layer4[0].conv2 = nn.Conv2d(
            512, 512, kernel_size=(3, 3), stride=(1, 2), padding=(1, 1), bias=False,
        )
        resnet.layer4[0].downsample[0] = nn.Conv2d(
            1024, 2048, kernel_size=(1, 1), stride=(1, 2), bias=False,
        )
        resnet.avgpool = AvgPoolingFreq()
        if embedding:
            resnet.fc = Identity(2048)
        else:
            resnet.fc = LinearSeq(2048, ResNetTranscription.output_size)

        return resnet

    def forward(self, x):
        x = x.unsqueeze(1)
        y = self.cnn(x)

        return torch.sigmoid(y)


with Settings.default.scope(ResNetTranscription.__scope__) as hparams:
    hparams.conv_net = "resnet18"
