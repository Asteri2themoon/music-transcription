import torch

import copy
import time


def meshgrid(*sizes):
    lst = [torch.arange(s) for s in sizes]
    return torch.meshgrid(*lst)


def batch_F1(theta):
    theta = theta.unsqueeze(1)

    f = theta >= 0.5
    likeli = torch.max(theta, 1 - theta)
    g = likeli == 1.0

    return f, likeli, g


def batch_index(likeli, g):
    bs, k, _ = likeli.shape

    likeli = copy.deepcopy(likeli)
    likeli[g] = 1.0
    index = torch.argmin(likeli, dim=2)

    i, j = meshgrid(bs, k)
    mask = likeli[i, j, index[i, j]] >= 1.0
    return index, mask


def batch_transform(f, likeli, g, index):
    bs, k, _ = likeli.shape

    i, j = meshgrid(bs, k)

    f_prime = copy.deepcopy(f)
    f_prime[i, j, index[i, j]] = ~f[i, j, index[i, j]]

    likeli_prime = copy.deepcopy(likeli)
    likeli_prime[i, j, index[i, j]] = 1 - likeli[i, j, index[i, j]]

    g_prime = copy.deepcopy(g)
    g_prime[i, j, index[i, j]] = ~g[i, j, index[i, j]]
    return f_prime, likeli_prime, g_prime


def batch_find_best(likeli_prime, mask):
    log_likeli = torch.prod(likeli_prime, dim=2)

    log_likeli[mask] = 0.0
    best = torch.argmax(log_likeli, dim=1).unsqueeze(1)

    return best


def batch_select(f_prime, likeli_prime, g_prime, best):
    bs = f_prime.shape[0]
    i, _ = meshgrid(bs, 1)
    return (
        f_prime[i, best[i, 0], :],
        likeli_prime[i, best[i, 0], :],
        g_prime[i, best[i, 0], :],
    )


def batch_mask_best_(g_k, best, index):
    bs = g_k.shape[0]

    (i,) = meshgrid(bs)

    g_k[i, 0, index[i, best[i].squeeze()]] = True


def batch_mask_index(f_prime, x_k):
    bs, k, _ = f_prime.shape
    i, j = meshgrid(bs, k)

    mask_index = torch.sum(f_prime[i, j] != x_k[i, 0], dim=2) == 0

    return mask_index


def batch_concat(t, t_k):
    return torch.cat([t, t_k], dim=1)


def batch_mask_index_(g, index, mask_index):
    bs, k, _ = g.shape
    i, j = meshgrid(bs, k)

    g[i, j, index[i, j]] |= mask_index[i, j]


def batch_likelihood_full(f, theta):
    k = f.shape[1]

    theta = theta.unsqueeze(1).repeat(1, k, 1)

    likelihood = f * theta + (~f) * (1 - theta)
    return likelihood


def batch_likelihood(f, theta):
    k = f.shape[1]

    theta = theta.unsqueeze(1).repeat(1, k, 1)

    likelihood = f * theta + (~f) * (1 - theta)
    return torch.sum(likelihood.log_(), dim=2)


def batch_topk_likelihood(theta, k):
    f, likeli, g = batch_F1(theta)

    for _ in range(1, k):
        index, mask = batch_index(likeli, g)

        f_prime, likeli_prime, g_prime = batch_transform(f, likeli, g, index)

        best = batch_find_best(likeli_prime, mask)

        x_k, l_k, g_k = batch_select(f_prime, likeli_prime, g_prime, best)

        mask_index = batch_mask_index(f_prime, x_k)

        batch_mask_index_(g, index, mask_index)

        batch_mask_best_(g_k, best, index)

        f = batch_concat(f, x_k)

        g = batch_concat(g, g_k)

        likeli = batch_concat(likeli, l_k)

    return f, batch_likelihood(f, theta)
