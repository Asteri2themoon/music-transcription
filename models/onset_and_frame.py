import torch
import torch.nn as nn
import torch.nn.functional as F

from torchtrain import DeepModel, Settings, HParams


class ConvNet(DeepModel):
    __scope__ = "model.convnet"

    def __init__(self, **kwargs):
        super(ConvNet, self).__init__(**kwargs)

        with self.scope() as p:
            self.time_bins = p.time_bins
            self.freq_bins = p.freq_bins

            self.conv = nn.Sequential(
                nn.Conv2d(1, p.conv.filter1, 3, padding=1),
                nn.BatchNorm2d(p.conv.filter1),
                nn.ReLU(),
                nn.Conv2d(p.conv.filter1, p.conv.filter1, 3, padding=1),
                nn.BatchNorm2d(p.conv.filter1),
                nn.ReLU(),
                nn.MaxPool2d((1, 2)),
                nn.Dropout2d(0.25),
                nn.Conv2d(p.conv.filter1, p.conv.filter2, 3, padding=1),
                nn.BatchNorm2d(p.conv.filter2),
                nn.ReLU(),
                nn.MaxPool2d((1, 2)),
                nn.Dropout2d(0.25),
            )

            self.fc = nn.Sequential(
                nn.Dropout(0.5),
                nn.Linear(p.freq_bins // 4 * p.time_bins * p.conv.filter2, 768),
                nn.ReLU(),
            )

    def forward(self, x):
        bs, channels, time_bins, freq_bins = x.shape

        assert freq_bins == self.freq_bins
        assert channels == 1

        conv = self.conv(x)

        pad_conv = F.pad(
            conv, (0, 0, self.time_bins // 2, self.time_bins // 2), "constant", 0
        )

        unfold = pad_conv.unfold(2, self.time_bins, 1)
        t_unfold = unfold.transpose_(3, 4).transpose_(1, 2)
        dense_input = t_unfold.reshape(
            bs * time_bins, conv.shape[1] * self.time_bins * conv.shape[3]
        )
        dense_output = self.fc(dense_input)
        output = dense_output.view(bs, time_bins, -1)

        return output


Settings.default.midi_pitch_min = 21
Settings.default.midi_pitch_max = 108

with Settings.default.scope(ConvNet.__scope__) as hparams:
    hparams.time_bins = 5
    hparams.freq_bins = 229
    hparams.conv.filter1 = 48
    hparams.conv.filter2 = 96
    hparams.linear = 768


class Onset(DeepModel):
    __scope__ = "model.onset"

    def __init__(self, **kwargs):
        super(Onset, self).__init__(**kwargs)

        with self.hparams.scope(ConvNet.__scope__) as p:
            convnet_output = p.linear

        self.notes = self.hparams.midi_pitch_max - self.hparams.midi_pitch_min + 1

        self.conv = ConvNet(**kwargs)

        with self.scope() as p:
            self.lstm = nn.LSTM(
                convnet_output,
                hidden_size=p.lstm_unit,
                num_layers=1,
                bidirectional=True,
            )
            self.lstm_output = p.lstm_unit * 2
            self.predict = nn.Sequential(
                nn.Linear(self.lstm_output, self.notes), nn.Sigmoid()
            )

    def forward(self, x):
        bs, _, time_bins, _ = x.shape
        conv = self.conv(x)
        lstm = self.lstm(conv)[0]
        allong_batch = lstm.view(bs * time_bins, self.lstm_output)
        output = self.predict(allong_batch)
        return output.view(bs, time_bins, self.notes)


with Settings.default.scope(Onset.__scope__) as hparams:
    hparams.lstm_unit = 256


class OnsetAndFrame(DeepModel):
    __scope__ = "model.onsetandframe"

    def __init__(self, **kwargs):
        super(OnsetAndFrame, self).__init__(**kwargs)

        with self.hparams.scope(ConvNet.__scope__) as p:
            convnet_output = p.linear

        self.onset = Onset(**kwargs)
        self.onset_last = None

        self.notes = self.hparams.midi_pitch_max - self.hparams.midi_pitch_min + 1

        self.conv = ConvNet(**kwargs)
        with self.scope() as p:
            self.stop_onset_gradient = p.stop_onset_gradient
            self.lstm_unit = p.lstm_unit

            self.in_fc = nn.Sequential(
                nn.Linear(convnet_output, self.notes), nn.Sigmoid()
            )
            self.lstm = nn.LSTM(
                self.notes * 2,
                hidden_size=self.lstm_unit,
                num_layers=1,
                bidirectional=True,
            )
            self.out_fc = nn.Sequential(
                nn.Linear(self.lstm_unit * 2, self.notes), nn.Sigmoid()
            )

    @property
    def onset_prediction(self):
        return self.onset_last

    def forward(self, x):
        bs, time_bins, freq_bins = x.shape

        self.onset_last = self.onset(x.view(bs, 1, time_bins, freq_bins))

        conv = self.conv(x.view(bs, 1, time_bins, freq_bins))

        allong_batch = conv.view(bs * time_bins, -1)
        in_fc = self.in_fc(allong_batch)

        onset = self.onset_last
        if self.stop_onset_gradient:
            onset = onset.clone().detach()

        in_lstm = torch.cat([in_fc.view(bs, time_bins, -1), onset], dim=2)

        lstm = self.lstm(in_lstm)[0]

        allong_batch = lstm.view(bs * time_bins, self.lstm_unit * 2)
        out_fc = self.out_fc(allong_batch)

        out = out_fc.view(bs, time_bins, self.notes)
        return out


with Settings.default.scope(OnsetAndFrame.__scope__) as hparams:
    hparams.lstm_unit = 256
    hparams.stop_onset_gradient = True
