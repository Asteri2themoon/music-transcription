import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable

from torchtrain import DeepModel, Settings

from .beam_search import beam_search, greedy_decoding
from .relative_attention import Transformer

"""
def allong_sequence(x, model):
    bs, seq_len, size = x.shape
    reshape_x = x.reshape(bs * seq_len, size)
    res = model(reshape_x)
    return res.view(bs, seq_len, -1)


class LinearClassifier(nn.Module):
    def __init__(self, z: int, n: int):
        super(LinearClassifier, self).__init__()

        self.layers = nn.Sequential(nn.ReLU(), nn.Linear(z, n, bias=True), nn.Sigmoid())

    def forward(self, z):
        return allong_sequence(z, self.layers)


class FeedForward(nn.Module):
    def __init__(self, z: int, h: int, dropout: float = 0.1, **kwargs):
        super(FeedForward, self).__init__()

        self.layers = nn.Sequential(
            nn.Linear(z, h, bias=True),
            nn.ReLU(),
            nn.Dropout(dropout),
            nn.Linear(h, z, bias=True),
        )

    def forward(self, z):
        return allong_sequence(z, self.layers)


class SelfAttention(nn.Module):
    def __init__(
        self,
        h: int,
        k: int,
        att: int,
        hs: int,
        dropout: float = 0.1,
        masked: bool = False,
    ):
        super(SelfAttention, self).__init__()

        self.masked = masked

        self.k = k
        l = self.k * 2 + 1
        d_h = att // h
        self.e_r = nn.Parameter(torch.randn(h, d_h, l), requires_grad=True)

        self.query = nn.Linear(hs, att)
        self.key = nn.Linear(hs, att)
        self.value = nn.Linear(hs, hs)
        self.out = nn.Linear(hs, hs)
        self.dropout = nn.Dropout(dropout)

        flip = torch.arange(k - 1, -1, -1, dtype=int)
        tri_mask = torch.ones(self.k + 1, self.k, dtype=bool).triu()[1:, :]

        self.mask_up = nn.Parameter(tri_mask[flip, :], requires_grad=False)
        self.mask_down = nn.Parameter(tri_mask[:, flip], requires_grad=False)

    def forward(self, query, key, value):
        query = allong_sequence(query, self.query)
        key = allong_sequence(key, self.key)
        value = allong_sequence(value, self.value)

        bs, seq_len, d = query.shape
        h, d_h, _ = self.e_r.shape

        assert d_h * h == d
        assert query.shape == key.shape
        # assert value.shape == key.shape

        sqrt_dh = torch.sqrt(torch.tensor(d_h, dtype=float))

        r_query = query.view(bs, seq_len, d_h, h).permute(0, 3, 1, 2)
        r_key = key.view(bs, seq_len, d_h, h).permute(0, 3, 1, 2)
        r_value = value.view(bs, seq_len, -1, h).permute(0, 3, 1, 2)

        e_r = self.e_r.repeat(bs, 1, 1, 1)

        q_e_r = torch.matmul(r_query, e_r)

        s_rel = self.skew(q_e_r)
        attention_weigths = torch.matmul(r_query, torch.transpose(r_key, -2, -1))

        if self.masked:
            mask = torch.ones_like(attention_weigths[0, 0], dtype=bool).triu().t_()
            attention_weigths.masked_fill_(~mask.repeat(bs, h, 1, 1), -float("inf"))

        softmax = F.softmax((attention_weigths + s_rel) / sqrt_dh, dim=-1)

        attention = torch.matmul(self.dropout(softmax), r_value)

        # concatenate all head
        z = torch.transpose(attention, 2, 1).reshape(bs, seq_len, -1)
        z = allong_sequence(z, self.out)
        return z

    def skew(self, x):
        bs, h, seq_len, _ = x.shape

        # apply mask
        r_mask_up = self.mask_up.repeat(bs, h, 1, 1)
        r_mask_down = self.mask_down.repeat(bs, h, 1, 1)
        x[:, :, : self.k, : self.k] *= r_mask_up
        x[:, :, -self.k :, -self.k :] *= r_mask_down

        # pad column
        x = F.pad(x, (0, seq_len - self.k), "constant", 0)

        # flatten and pad
        x = x.view(bs, h, -1)
        x = F.pad(x, (0, self.k), "constant", 0)

        # reshape
        x = x.reshape(bs, h, -1, seq_len + self.k)

        # slice
        x = x[:, :, :-1, self.k :]
        return x


class EncoderLayer(nn.Module):
    def __init__(
        self,
        r: int,
        att: int = 256,
        hs: int = 512,
        ff: int = 512,
        h: int = 8,
        dropout: float = 0.1,
    ):
        super(EncoderLayer, self).__init__()
        self.self_att = SelfAttention(h, r, att=att, hs=hs, dropout=dropout)
        self.ff = FeedForward(hs, ff, dropout=dropout)

    def forward(self, x):
        att = self.self_att(x, x, x)
        att = F.layer_norm(x + att, x.shape[2:])

        # z = allong_sequence(att, self.ff)
        z = self.ff(att)
        z = F.layer_norm(att + z, x.shape[2:])
        return z


class Encoder(nn.Module):
    def __init__(
        self,
        input_size: int,
        length: int,
        r: int,
        hs: int = 512,
        att: int = 256,
        ff: int = 1024,
        h: int = 8,
        dropout: float = 0.1,
    ):
        super(Encoder, self).__init__()

        layers = []
        for _ in range(length):
            l = EncoderLayer(r=r, att=att, hs=hs, ff=ff, h=h, dropout=dropout,)
            layers.append(l)

        self.encoder = nn.Sequential(*layers)

    def forward(self, x):
        return self.encoder(x)


class DecoderLayer(nn.Module):
    def __init__(
        self,
        r: int,
        att: int = 256,
        hs: int = 512,
        ff: int = 512,
        h: int = 8,
        dropout: float = 0.1,
    ):
        super(DecoderLayer, self).__init__()
        self.self_att = SelfAttention(
            h, r, att=att, hs=hs, dropout=dropout, masked=True
        )
        self.self_att2 = SelfAttention(h, r, att=att, hs=hs, dropout=dropout)
        self.ff = FeedForward(hs, ff, dropout=dropout)

    def forward(self, x, enc_output):
        att = self.self_att(x, x, x)
        att = F.layer_norm(x + att, x.shape[2:])

        att2 = self.self_att2(att, enc_output, enc_output)
        att2 = F.layer_norm(att + att2, x.shape[2:])

        # z = allong_sequence(att2, self.ff)
        z = self.ff(att2)
        z = F.layer_norm(att2 + z, att2.shape[2:])
        return z


class Decoder(nn.Module):
    def __init__(
        self,
        output_size: int,
        length: int,
        r: int,
        hs: int = 512,
        att: int = 256,
        ff: int = 1024,
        h: int = 8,
        dropout: float = 0.1,
    ):
        super(Decoder, self).__init__()

        self.embedding = nn.Linear(output_size, hs)
        self.output = nn.Sequential(nn.Linear(hs, output_size), nn.Sigmoid())
        layers = []
        for _ in range(length):
            l = DecoderLayer(r=r, att=att, hs=hs, ff=ff, h=h, dropout=dropout,)
            layers.append(l)

        self.decoder = nn.ModuleList(layers)

    def forward(self, x, enc_output):
        z = allong_sequence(x, self.embedding)
        for l in self.decoder:
            z = l(z, enc_output)
        out = allong_sequence(z, self.output)
        return out


class MusicTransformer(DeepModel):
    __scope__ = "model.musictransformer"

    def __init__(self, **kwargs):
        super(MusicTransformer, self).__init__(**kwargs)

        with self.scope() as p:
            self.embedding = nn.Linear(p.input_size, p.hs)
            self.output_size = p.output_size
            self.encoder = Encoder(
                input_size=p.input_size,
                length=p.length,
                r=p.r,
                hs=p.hs,
                att=p.att,
                ff=p.ff,
                h=p.h,
                dropout=p.dropout,
            )
            self.decoder = Decoder(
                output_size=p.output_size,
                length=p.length,
                r=p.r,
                hs=p.hs,
                att=p.att,
                ff=p.ff,
                h=p.h,
                dropout=p.dropout,
            )

    def forward(self, x, y=None):
        emb = allong_sequence(x, self.embedding)

        if y is not None:  # teacher forcing
            emb = F.pad(emb, (0, 0, 1, 0), "constant", 0)
            y = F.pad(y, (0, 0, 1, 0), "constant", 0)
            enc = self.encoder(emb)
            y = self.decoder(y, enc)[:, 1:, :]
        else:
            raise Exception("inference not implemented yet")

        return y


with Settings.default.scope(MusicTransformer.__scope__) as hparams:
    hparams.input_size = 88
    hparams.length = 8
    hparams.r = 4
    hparams.hs = 512
    hparams.att = 256
    hparams.ff = 1024
    hparams.h = 8
    hparams.teacher_forcing = True
    hparams.output_size = 88
    hparams.dropout = 0.1


class ConvNet(nn.Module):
    def __init__(
        self,
        conv1: int = 48,
        conv2: int = 96,
        z: int = 512,
        freq_bins: int = 229,
        conv_dropout: float = 0.25,
        linear_dropout: float = 0.50,
    ):
        super(ConvNet, self).__init__()

        self.time_bins = 5
        self.freq_bins = freq_bins

        self.conv = nn.Sequential(
            nn.Conv2d(1, conv1, 3, padding=1),
            nn.BatchNorm2d(conv1),
            nn.ReLU(),
            nn.Conv2d(conv1, conv1, 3, padding=1),
            nn.BatchNorm2d(conv1),
            nn.ReLU(),
            nn.MaxPool2d((1, 2)),
            nn.Dropout2d(conv_dropout),
            nn.Conv2d(conv1, conv2, 3, padding=1),
            nn.BatchNorm2d(conv2),
            nn.ReLU(),
            nn.MaxPool2d((1, 2)),
            nn.Dropout2d(conv_dropout),
        )

        self.fc = nn.Sequential(
            nn.Dropout(linear_dropout),
            nn.Linear(freq_bins // 4 * self.time_bins * conv2, z),
            nn.ReLU(),
        )

    def forward(self, x):
        bs, time_bins, freq_bins = x.shape

        assert freq_bins == self.freq_bins

        conv = self.conv(x.unsqueeze(1))

        pad_conv = F.pad(
            conv, (0, 0, self.time_bins // 2, self.time_bins // 2), "constant", 0
        )

        unfold = pad_conv.unfold(2, self.time_bins, 1)
        t_unfold = unfold.transpose_(3, 4).transpose_(1, 2)
        dense_input = t_unfold.reshape(
            bs * time_bins, conv.shape[1] * self.time_bins * conv.shape[3]
        )
        dense_output = self.fc(dense_input)
        output = dense_output.view(bs, time_bins, -1)

        return output


class AllConv(nn.Module):
    def __init__(
        self,
        conv1: int = 48,
        conv2: int = 96,
        conv3: int = 192,
        z: int = 512,
        dropout: float = 0.25,
    ):
        super(AllConv, self).__init__()

        self.z = z
        self.conv = nn.Sequential(
            # first stage
            nn.Conv2d(1, conv1, 3, padding=(1, 0)),
            nn.BatchNorm2d(conv1),
            nn.ReLU(),
            nn.Conv2d(conv1, conv1, 3, padding=(1, 0)),
            nn.BatchNorm2d(conv1),
            nn.ReLU(),
            nn.MaxPool2d((1, 2)),
            nn.Dropout2d(dropout),
            # second stage
            nn.Conv2d(conv1, conv1, (1, 3)),
            nn.BatchNorm2d(conv1),
            nn.ReLU(),
            nn.Conv2d(conv1, conv1, (1, 3)),
            nn.BatchNorm2d(conv1),
            nn.ReLU(),
            nn.MaxPool2d((1, 2)),
            nn.Dropout2d(dropout),
            # third stage
            nn.Conv2d(conv1, conv2, (1, 25)),
            nn.BatchNorm2d(conv2),
            nn.ReLU(),
            nn.Conv2d(conv2, conv3, (1, 25)),
            nn.BatchNorm2d(conv3),
            nn.ReLU(),
            # fourth stage
            nn.Conv2d(conv3, z, 1),
            nn.BatchNorm2d(z),
        )

    def forward(self, x):
        bs, time_bins, freq_bins = x.shape

        # spectrogram to 1 channel image
        reshaped_x = x.view(bs, 1, time_bins, freq_bins)
        # allConv
        y = self.conv(reshaped_x)
        y = F.relu(F.avg_pool2d(y, (1, y.shape[-1])))

        # sequence of tokens
        emb = torch.transpose(y.view(bs, self.z, time_bins), 1, 2)

        return emb


class Linear(nn.Module):
    def __init__(self, z: int, freq_bins: int = 229):
        super(Linear, self).__init__()

        self.linear = nn.Sequential(nn.Linear(freq_bins, z, bias=True), nn.Tanh(),)

    def forward(self, z):
        return allong_sequence(z, self.linear)


class TranscriptionTransformer(DeepModel):
    __scope__ = "model.transcriptiontransformer"

    def __init__(self, **kwargs):
        super(TranscriptionTransformer, self).__init__(**kwargs)

        freq_bins = 229

        with self.scope() as p:
            self.type = p.embedding.type
            assert self.type in ["linear", "all_conv", "conv_net"]

            self.intermediary_loss = p.embedding.intermediary_loss
            self.output_size = p.trans.output_size

            if self.type == "linear":
                self.embedding = Linear(z=p.trans.hs, freq_bins=freq_bins)
            elif self.type == "all_conv":
                self.embedding = AllConv(
                    conv1=p.embedding.all_conv.conv1,
                    conv2=p.embedding.all_conv.conv2,
                    conv3=p.embedding.all_conv.conv3,
                    z=p.trans.hs,
                    dropout=p.embedding.all_conv.dropout,
                )
            elif self.type == "conv_net":
                self.embedding = ConvNet(
                    freq_bins=freq_bins,
                    z=p.trans.hs,
                    conv1=p.embedding.conv_net.conv1,
                    conv2=p.embedding.conv_net.conv2,
                    conv_dropout=p.embedding.conv_net.conv_dropout,
                    linear_dropout=p.embedding.conv_net.linear_dropout,
                )
            else:
                raise Exception("unknown type of embedding")

            if self.intermediary_loss:
                self.intermediary = LinearClassifier(z=p.trans.hs, n=self.output_size)
            else:
                self.intermediary = None

            self.hidden_size = p.trans.hs

            self.encoder = Encoder(
                input_size=p.trans.hs,
                length=p.trans.length,
                r=p.trans.r,
                hs=p.trans.hs,
                att=p.trans.att,
                ff=p.trans.ff,
                h=p.trans.h,
                dropout=p.trans.dropout,
            )
            self.decoder = Decoder(
                output_size=p.trans.output_size,
                length=p.trans.length,
                r=p.trans.r,
                hs=p.trans.hs,
                att=p.trans.att,
                ff=p.trans.ff,
                h=p.trans.h,
                dropout=p.trans.dropout,
            )

    def forward(self, x, y=None, k=0):
        emb = self.embedding(x)

        if y is not None:  # teacher forcing
            x = F.pad(emb.clone().detach(), (0, 0, 1, 0), "constant", 0)
            y = F.pad(y, (0, 0, 1, 0), "constant", 0)
            enc = self.encoder(x)
            y = self.decoder(y, enc)[:, 1:, :]
            if self.intermediary is not None:
                return y, self.intermediary(emb)
            else:
                return y, None
        else:
            enc = self.encoder(emb)
            if k == 0:
                y = greedy_decoding(self.decoder, enc, self.output_size)
            else:
                y = beam_search(self.decoder, enc, self.output_size, k)

            return y, None


with Settings.default.scope(TranscriptionTransformer.__scope__) as hparams:
    hparams.embedding.type = "linear"  # linear, all_conv, conv_net
    hparams.embedding.all_conv.conv1 = 48
    hparams.embedding.all_conv.conv2 = 96
    hparams.embedding.all_conv.conv3 = 192
    hparams.embedding.all_conv.dropout = 0.25
    hparams.embedding.conv_net.conv1 = 48
    hparams.embedding.conv_net.conv2 = 96
    hparams.embedding.conv_net.conv_dropout = 0.25
    hparams.embedding.conv_net.linear_dropout = 0.50
    hparams.embedding.intermediary_loss = False
    hparams.trans.length = 6
    hparams.trans.r = 256
    hparams.trans.hs = 512
    hparams.trans.att = 256
    hparams.trans.ff = 2048
    hparams.trans.h = 8
    hparams.trans.output_size = 88
    hparams.trans.dropout = 0.1
"""

import math
import torch
import torch.nn as nn
import torch.nn.functional as F

from torchtrain import DeepModel, Settings


class PositionalEncoding(nn.Module):
    def __init__(self, d_model, dropout=0.1, max_len=5000):
        super(PositionalEncoding, self).__init__()
        self.dropout = nn.Dropout(p=dropout)

        pe = torch.zeros(max_len, d_model)
        position = torch.arange(0, max_len, dtype=torch.float).unsqueeze(1)
        div_term = torch.exp(
            torch.arange(0, d_model, 2).float() * (-math.log(10000.0) / d_model)
        )
        pe[:, 0::2] = torch.sin(position * div_term)
        pe[:, 1::2] = torch.cos(position * div_term)
        pe = pe.unsqueeze(0).transpose(0, 1)
        self.register_buffer("pe", pe)

    def forward(self, x):
        x = x + self.pe[: x.size(0), :]
        return self.dropout(x)


class TranscriptionTransformer(DeepModel):
    __scope__ = "model.transcription_transformer"

    def __init__(self, **kwargs):
        super(TranscriptionTransformer, self).__init__(**kwargs)

        with self.scope() as p:
            self.vocabulary = p.output.vocabulary
            self.d_model = p.d_model
            self.token_dim = p.output.dim

            if p.input.dim != self.d_model:
                self.encoder_embedding = nn.Linear(p.input.dim, p.d_model)
            else:
                self.encoder_embedding = None

            if p.relative_attention <= 0:
                self.pos_encoder = PositionalEncoding(p.d_model, p.dropout_embedding)
            else:
                self.pos_encoder = None

            self.transformer = Transformer(
                d_model=p.d_model,
                nhead=p.nhead,
                k=p.relative_attention,
                num_encoder_layers=p.num_encoder_layers,
                num_decoder_layers=p.num_decoder_layers,
                dim_feedforward=p.dim_feedforward,
                dropout=p.dropout,
            )
            self.decoder_embedding = nn.Linear(p.output.dim, p.d_model)
            self.decoder_pred = nn.Linear(p.d_model, p.output.dim)

        self.mask = None

    def forward(self, x, y=None):
        bs, length, _ = x.shape

        if self.mask is None or self.mask.shape[0] != length:
            self.mask = self.transformer.generate_square_subsequent_mask(length).to(
                x.device
            )

        if self.encoder_embedding is not None:
            x_emb = self.encoder_embedding(x)
        else:
            x_emb = x

        x_emb = x_emb.transpose(0, 1)

        if self.pos_encoder is None:
            x_pos_emb = x_emb
        else:
            x_pos_emb = self.pos_encoder(x_emb)  # .view(length, bs, self.d_model)

        if y is None:  # greedy decoding
            across_batch = torch.arange(0, bs, dtype=int)
            y = x.new(length, bs, self.token_dim).zero_()

            y_emb = self.decoder_embedding(y)
            if self.pos_encoder is None:
                y_pos_emb = y_emb
            else:
                y_pos_emb = self.pos_encoder(y_emb)

            decoded = self.transformer(x_pos_emb, y_pos_emb, tgt_mask=self.mask)

            out = torch.argmax(self.decoder_pred(decoded)[:, 0], dim=1)
            y[0, across_batch, out[across_batch]] = 1.0  # update init token

            for i in range(length - 1):
                y_emb = self.decoder_embedding(y)

                if self.pos_encoder is None:
                    y_pos_emb = y_emb
                else:
                    y_pos_emb = self.pos_encoder(y_emb)

                decoded = self.transformer(x_pos_emb, y_pos_emb, tgt_mask=self.mask)

                out = self.decoder_pred(decoded)
                if self.vocabulary:
                    out = torch.argmax(out[i, :], dim=1)
                    y[i + 1, across_batch, out[across_batch]] = 1.0
                else:
                    y[i + 1, across_batch] = (torch.sigmoid(out[i, :]) >= 0.5).float()

        else:  # teacher forcing
            y = y.transpose(0, 1)
            y_emb = self.decoder_embedding(y)
            if self.pos_encoder is None:
                y_pos_emb = y_emb
            else:
                y_pos_emb = self.pos_encoder(y_emb)

            decoded = self.transformer(x_pos_emb, y_pos_emb, tgt_mask=self.mask)

            y = self.decoder_pred(decoded)

        return y.transpose(0, 1)


with Settings.default.scope(TranscriptionTransformer.__scope__) as hparams:
    hparams.d_model = 512
    hparams.nhead = 8
    hparams.num_encoder_layers = 6
    hparams.num_decoder_layers = 6
    hparams.dim_feedforward = 2048
    hparams.dropout = 0.3
    hparams.dropout_embedding = 0.3
    hparams.relative_attention = 256

    hparams.input.dim = 512
    hparams.output.vocabulary = True
    hparams.output.dim = 388

