import torch

from .likelihood import batch_topk_likelihood


def k_decoder(decoder, y, enc):
    bs, k, length, token_size = y.shape

    y = y.view(bs * k, length, token_size)
    enc = enc.unsqueeze(1).repeat(1, k, 1, 1).view(bs * k, length, -1)
    theta = decoder(y, enc).detach()
    theta = theta.view(bs, k, length, token_size)
    return theta


def select_topk_follower(log_likelihood, likelihood, k):
    bs = log_likelihood.shape[0]

    prev_likelihood = log_likelihood.unsqueeze(2).repeat(1, 1, k)

    _, index = torch.topk((prev_likelihood + likelihood).view(bs, -1), k=k, dim=1)

    bs_index_2d = []
    for b in range(bs):
        index_2d = []
        for j in range(k):
            seq, alt = index[b, j] // k, index[b, j] % k
            index_2d.append([seq, alt])
        bs_index_2d.append(index_2d)
    bs_index_2d = torch.tensor(bs_index_2d)

    return bs_index_2d[:, :, 0], bs_index_2d[:, :, 1]


def greedy_decoding(decoder, enc, token_size):
    bs, length, _ = enc.shape

    seq = enc.new(bs, length, token_size).zero_().requires_grad_(False)

    theta = decoder(seq[:, :, :], enc).detach()[:, 0, :]

    seq[:, 0, :] = theta >= 0.5

    for i in range(0, length - 1):
        # print(f"{i+1}/{length}", end="\r")
        theta = decoder(seq[:, :, :], enc).detach()[:, i + 1, :]
        seq[:, i + 1, :] = theta >= 0.5

    return seq


def beam_search(decoder, enc, token_size, k):
    bs, length, _ = enc.shape

    accros_batch = torch.arange(0, bs, dtype=int)
    accros_k = torch.arange(0, k, dtype=int)
    mesh_batch, mesh_k = torch.meshgrid(accros_batch, accros_k)

    beam = enc.new(bs, k, length, token_size).zero_().requires_grad_(False)

    log_likelihood = enc.new(bs, k).zero_().requires_grad_(False)

    theta = k_decoder(decoder, beam[:, :1, :, :], enc).detach()[:, :, 0, :]

    topk, likelihood = batch_topk_likelihood(theta.view(bs, token_size), k)
    topk = topk.view(bs, 1, k, token_size)
    likelihood = likelihood.view(bs, 1, k)

    beam[:, :, 0, :] = topk[:, 0, :, :]
    log_likelihood[:, :] = likelihood[:, 0, :]

    for i in range(0, length - 1):
        # print(f"{i+1}/{length}", end="\r")
        theta = k_decoder(decoder, beam, enc).detach()[:, :, i + 1, :]
        topk, likelihood = batch_topk_likelihood(theta.view(bs * k, token_size), k)
        topk = topk.view(bs, k, k, token_size)
        likelihood = likelihood.view(bs, k, k)

        seq, alt = select_topk_follower(log_likelihood[:, :], likelihood, k)

        beam[mesh_batch, mesh_k] = beam[mesh_batch, seq[mesh_batch, mesh_k]]

        log_likelihood[mesh_batch, mesh_k] = log_likelihood[
            mesh_batch, seq[mesh_batch, mesh_k]
        ]

        # append new tokens
        beam[mesh_batch, mesh_k, i + 1] = topk[
            mesh_batch, seq[mesh_batch, mesh_k], alt[mesh_batch, mesh_k]
        ].float()
        log_likelihood[mesh_batch, mesh_k] += likelihood[
            mesh_batch, seq[mesh_batch, mesh_k], alt[mesh_batch, mesh_k]
        ]

    log_likelihood, argmax = torch.max(log_likelihood, dim=1)

    seq = beam[accros_batch, argmax, :]

    return seq, log_likelihood
