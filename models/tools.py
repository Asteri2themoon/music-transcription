import torch
import torch.nn as nn
import torch.nn.functional as F


class AvgPoolingFreq(nn.Module):
    def __init__(self):
        super(AvgPoolingFreq, self).__init__()

    def forward(self, x):
        x = F.avg_pool2d(x, (1, x.shape[-1]), stride=None, padding=0)
        return x


class LinearSeq(nn.Module):
    def __init__(self, z: int, n: int):
        super(LinearSeq, self).__init__()

        self.z = z
        self.n = n
        self.linear = nn.Linear(z, n, bias=False)

    def forward(self, x):
        batch, tmp = x.shape
        length = tmp // self.z

        x = x.view(batch, self.z, -1)

        x = torch.transpose(x, 1, 2).reshape(batch * length, self.z)
        y = self.linear(x)

        return y.view(batch, length, self.n)


class Conv2Embedding(nn.Module):
    def __init__(self, z: int, n: int):
        super(Conv2Embedding, self).__init__()

        self.z = z
        self.n = n
        self.linear = nn.Linear(z, n, bias=False)

    def forward(self, x):
        batch, features, length, width = x.shape
        assert (features * width) == self.z

        x = torch.transpose(x, 1, 2).reshape(batch * length, self.z)
        y = self.linear(x)

        return y.view(batch, length, self.n)


class Identity(nn.Module):
    def __init__(self, z: int):
        super(Identity, self).__init__()

        self.z = z

    def forward(self, x):
        batch, tmp = x.shape
        length = tmp // self.z

        x = x.view(batch, length, self.z)

        return x
