import torch
import torch.nn as nn
import torch.nn.functional as f

import math

from torchtrain import DeepModel, Settings

from .tools import Conv2Embedding


class ConvNet(DeepModel):
    __scope__ = "model.convnet_embedding"
    output_size = 88

    def __init__(self, embedding: bool = False, **kwargs):
        super(ConvNet, self).__init__(**kwargs)

        self.embedding_mode = embedding

        with self.scope() as p:
            self.embedding = nn.Sequential(
                nn.Conv2d(1, 48, 3, padding=1),
                nn.BatchNorm2d(48),
                nn.ReLU(),
                nn.Conv2d(48, 48, 3, padding=1),
                nn.BatchNorm2d(48),
                nn.ReLU(),
                nn.MaxPool2d((1, 2)),
                nn.Dropout2d(0.25),
                nn.Conv2d(48, 96, 3, padding=1),
                nn.BatchNorm2d(96),
                nn.ReLU(),
                nn.MaxPool2d((1, 2)),
                nn.Dropout2d(0.25),
                Conv2Embedding(229 // 4 * 96, p.embedding_size),
            )
            self.classifier = nn.Sequential(
                nn.Dropout(0.5),
                nn.Linear(p.embedding_size, self.output_size),
                nn.Sigmoid(),
            )

    def forward(self, x):
        x = x.unsqueeze(1)
        y = self.embedding(x)

        if not self.embedding_mode:
            y = self.classifier(y)

        return y


with Settings.default.scope(ConvNet.__scope__) as hparams:
    hparams.embedding_size = 512

