import torch
import torch.nn.functional as F
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches

# vocabulary config
notes = 128
times = 100
delta_t = 0.01
velocity = 32


def pitch2freq(pitch, A4: float = 440.0):
    return torch.pow(2, (pitch - 69.0) / 12) * A4


def freq2pitch(f):
    return 12 * (np.log(f / 440.0) / np.log(2)) + 69


midi_freq = pitch2freq(torch.arange(128.0))


def pool_labels2mir_eval(args):
    onsets, offsets = args
    return labels2mir_eval(onsets, offsets)


def labels2mir_eval(onsets, offsets, midi_offset: int = 21, timestep: float = 0.032):
    assert len(onsets.shape) == 2
    assert offsets.shape == onsets.shape

    t0 = -torch.ones(onsets.shape[1], dtype=float)
    intervals, pitchs = [], []

    for i in range(onsets.shape[0]):
        t = i * timestep

        # offset
        c_offsets = offsets[i].nonzero()
        for offset in c_offsets:
            if t0[offset] >= 0:
                intervals.append([t0[offset].item(), t])
                pitchs.append(midi_freq[offset + midi_offset].item())
            t0[offset] = -1.0

        # onset
        c_onsets = onsets[i].nonzero()
        for onset in c_onsets:
            if t0[onset] >= 0:
                intervals.append([t0[onset].item(), t])
                pitchs.append(midi_freq[onset + midi_offset].item())
            t0[onset] = t

    return torch.tensor(intervals).view(-1, 2), torch.tensor(pitchs)


def frames2onsets_offsets(frames):
    diff = (frames[:, 1:] - frames[:, :-1]) != 0
    act_frames = frames != 0

    onsets = act_frames[:, 1:] & diff
    offsets = ~act_frames[:, 1:] & diff
    offsets[:, 1:] |= (
        act_frames[:, :-2] & diff[:, :-1] & (act_frames[:, 1:-1] | ~diff[:, :-1])
    )

    onsets = F.pad(onsets, (0, 0, 1, 0))
    offsets = F.pad(offsets, (0, 0, 0, 1))

    return onsets, offsets


def frames2mir_eval(frames, midi_offset: int = 21, timestep: float = 0.032):
    diff = (frames[1:] - frames[:-1]) != 0
    act_frames = frames != 0

    onsets = act_frames[1:] & diff
    offsets = ~act_frames[1:] & diff
    offsets[1:] |= act_frames[:-2] & diff[:-1] & (act_frames[1:-1] | ~diff[:-1])

    onsets = F.pad(onsets, (0, 0, 1, 0))
    offsets = F.pad(offsets, (0, 0, 0, 1))

    return labels2mir_eval(onsets, offsets, midi_offset, timestep)


def labels2vocabulary(
    onsets,
    offsets,
    midi_offset: int = 21,
    timestep: float = 0.032,
    include_velocity: bool = True,
):
    assert len(onsets.shape) == 2
    assert offsets.shape == onsets.shape

    # offset
    EVENT_ON = 0
    EVENT_OFF = EVENT_ON + notes
    TIME_SHIFT = EVENT_OFF + notes
    VELOCITY = TIME_SHIFT + times

    def append_timeshift(seq, prev_t, t):
        time_shift = int(round((t - prev_t) / delta_t))
        while time_shift > 0:
            if time_shift > times:
                current_shift = times - 1
                time_shift -= times
            else:
                current_shift = time_shift - 1
                time_shift = 0
            seq.append(TIME_SHIFT + current_shift)
            prev_t += delta_t * (current_shift + 1)

        return prev_t

    seq = []
    prev_t = 0
    prev_v = 0

    for i in range(onsets.shape[0]):
        t = i * timestep

        # offset
        c_offsets = offsets[i].nonzero()
        if len(c_offsets) > 0:  # time shift
            prev_t = append_timeshift(seq, prev_t, t)

        for offset in c_offsets:  # note off event
            seq.append(EVENT_OFF + int(offset) + midi_offset)

        # onset
        c_onsets = onsets[i].nonzero()
        if len(c_onsets) > 0:  # time shift
            prev_t = append_timeshift(seq, prev_t, t)

        for onset in c_onsets:  # note on event
            if include_velocity:  # velocity change event
                vel = int((onsets[i, onset] * velocity) - 1)
                assert 0 <= vel < velocity
                if vel != prev_v:
                    seq.append(VELOCITY + vel)
                    prev_v = vel
            seq.append(EVENT_ON + int(onset) + midi_offset)

    return torch.tensor(seq)


def vocabulary2mir_eval(seq):
    t0 = -torch.ones(128, dtype=float)
    intervals, pitchs = [], []

    # offset
    EVENT_ON = 0
    EVENT_OFF = EVENT_ON + notes
    TIME_SHIFT = EVENT_OFF + notes
    VELOCITY = TIME_SHIFT + times

    c_time = 0

    for token in seq:
        if token < EVENT_OFF:  # EVENT_ON
            note = int(token)
            t0[note] = c_time
        elif token < TIME_SHIFT:  # EVENT_OFF
            note = int(token - EVENT_OFF)
            if t0[note] >= 0.0:
                if t0[note].item() < c_time:
                    intervals.append([t0[note].item(), c_time])
                    pitchs.append(midi_freq[note])
        elif token < VELOCITY:  # TIME_SHIFT
            time_shift = int(token - TIME_SHIFT + 1) * delta_t
            c_time += time_shift
        else:  # VELOCITY
            pass

    return torch.tensor(intervals).view(-1, 2), torch.tensor(pitchs)


def plot_mireval(ax, ref_intervals, ref_pitches, color=None):
    if color is None:
        color_wheel = plt.rcParams["axes.prop_cycle"].by_key()["color"]
        if len(ax.lines) > 0:
            prev_color = color_wheel.index(ax.lines[-1].get_color())
        else:
            prev_color = len(color_wheel) - 1
        color = color_wheel[(prev_color + 1) % len(color_wheel)]

    for (begin, end), freq in zip(ref_intervals, ref_pitches):
        pitch = freq2pitch(freq).round()
        ax.add_patch(
            patches.Rectangle(
                (begin, pitch - 0.5),
                end - begin,
                1,
                linewidth=1,
                edgecolor="none",
                facecolor=color,
            )
        )
        ax.plot([begin, end], [pitch, pitch], color=color, alpha=0)


"""
data = MaestroDataset(
    "/var/dataset/maestro-v2.0.0-prepro/maestro-v2.0.0.json", 1, num_workers=16
)

import mir_eval

total_fmesure = 0
total_sample = 0
for i, batch in enumerate(data.train):
    print(f"progress: {(i+1)*100/len(data.train):.1f}%", end="\r")
    seq = labels2vocabulary(batch.onsets[0], batch.offsets[0])
    intervals_label, pitchs_label = labels2mir_eval(batch.onsets[0], batch.offsets[0])
    intervals_voc, pitchs_voc = vocabulary2mir_eval(seq)

    try:
        scores = mir_eval.transcription.evaluate(
            intervals_label.numpy(),
            pitchs_label.numpy(),
            intervals_voc.numpy(),
            pitchs_voc.numpy(),
        )
        total_fmesure += scores["F-measure"]
        total_sample += 1
    except ValueError:
        pass

    plt.subplot(311)
    plt.imshow(batch.frames[0, :, torch.arange(87, -1, -1, dtype=int)].t_())
    plt.subplot(312)
    plot_mireval(plt.gca(), notes_label, pitchs_label)
    plt.xlim((0, 20))
    plt.subplot(313)
    plot_mireval(plt.gca(), notes_voc, pitchs_voc)
    plt.xlim((0, 20))
    plt.show()


print(f"final score:{total_fmesure*100/total_sample:.2f}%")
"""
