import torch
import torch.optim as optim
import torch.nn as nn
import torch.nn.functional as F

import matplotlib

matplotlib.use("Agg")

from torchtrain import Training, Settings, HParams

from metrics import (
    LossMetric,
    ClassificationMetric,
    GlobalClassificationMetric,
    MAPMetric,
)
from models import OnsetAndFrame
from dataset import MaestroDataset

import os


class TrainOnsetsAndFrame(Training):
    __scope__ = "training.onsetsandframes"

    def __init__(self, **kwargs):
        super(TrainOnsetsAndFrame, self).__init__(**kwargs)

        self.metrics.add_metrics(
            ["train", "validate", "test"], LossMetric(device=self.device)
        )
        self.metrics.add_metrics(
            ["validate", "test"],
            ClassificationMetric(num_class=88, device=self.device),
        )
        self.metrics.add_metrics(
            ["validate", "test"], GlobalClassificationMetric(device=self.device),
        )
        self.metrics.add_metrics(
            ["validate", "test"], MAPMetric(num_class=88, device=self.device),
        )

    @staticmethod
    def exp_lr_scheduler(
        optimizer, global_step, init_lr, decay_steps, decay_rate, staircase=True,
    ):
        """Decay learning rate by a factor of 0.1 every lr_decay_epoch epochs."""
        if staircase:
            lr = init_lr * decay_rate ** (global_step // decay_steps)
        else:
            lr = init_lr * decay_rate ** (global_step / decay_steps)

        for param_group in optimizer.param_groups:
            param_group["lr"] = lr

    # init or load dataset
    def init(
        self,
        hparams: Settings,
        dataset_path: str,
        train: bool = True,
        validation: bool = True,
        test: bool = True,
    ):
        with hparams.scope(self.__scope__) as p:
            self.batch_size = p.batch_size
            self.epoch = p.epoch

        self.model = OnsetAndFrame(**hparams.params)

        self.model = self.model.to(self.device)

        with hparams.scope(self.__scope__) as p:
            self.lr = p.lr
            self.decay_steps = p.decay_steps
            self.decay_rate = p.decay_rate
            self.optimizer = [optim.Adam(self.model.parameters(), lr=p.lr)]

        # setup data loader
        with hparams.scope(self.__scope__) as p:
            loader = MaestroDataset(
                dataset_path, batch_size=self.batch_size, num_workers=self.num_workers
            )
            self.train_loader = loader.train
            self.validation_loader = loader.validation
            self.test_loader = loader.test
            self.step_per_epoch = len(self.train_loader)

    # train on one batch
    def train_batch(self, data, model, optimizer, loss, epoch, batch):
        optimizer[0].zero_grad()
        global_step = self.step_per_epoch * epoch + batch
        self.exp_lr_scheduler(
            optimizer[0],
            global_step,
            init_lr=self.lr,
            decay_steps=self.decay_steps,
            decay_rate=self.decay_rate,
            staircase=False,
        )

        spec = data.spec.to(self.device)
        frames = data.frames.to(self.device)
        onsets = data.onsets.to(self.device)
        # offsets = data.offsets.to(self.device)
        weigths = data.weigths.to(self.device)

        pred = model(spec)
        l_onset = F.binary_cross_entropy(model.onset_prediction, onsets)
        l_frame = F.binary_cross_entropy(pred, frames, weight=weigths)
        loss = l_onset + l_frame

        loss.backward()
        optimizer[0].step()

        res = {"loss": loss, "prediction": pred, "ground_truth": frames}

        return res

    # validate on one batch
    def validate_batch(self, data, model, optimizer, loss, epoch, batch):
        spec = data.spec.to(self.device)
        frames = data.frames.to(self.device)
        onsets = data.onsets.to(self.device)
        # offsets = data.offsets.to(self.device)
        weigths = data.weigths.to(self.device)

        pred = model(spec)
        l_onset = F.binary_cross_entropy(model.onset_prediction, onsets)
        l_frame = F.binary_cross_entropy(pred, frames, weight=weigths)
        loss = l_onset + l_frame

        res = {"loss": loss, "prediction": pred, "ground_truth": frames}

        return res

    # test on one batch
    def test_batch(self, data, model, optimizer, loss, batch):
        return self.validate_batch(data, model, optimizer, loss, 0, batch)


with Settings.default.scope(TrainOnsetsAndFrame.__scope__) as hparams:
    hparams.batch_size = 8
    hparams.epoch = 200
    hparams.lr = 6e-4
    hparams.decay_steps = 10000
    hparams.decay_rate = 0.98
