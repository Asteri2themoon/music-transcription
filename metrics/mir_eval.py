import torch
from torch.utils.tensorboard import SummaryWriter
import numpy as np
import mir_eval

from torchtrain import Metric

import time
import json
import os

import sys

sys.path.append("..")
from utils import plot_mireval


class MIREvalMetric(Metric):
    __name__ = "metrics.mir_eval"

    def __init__(self, **kwargs):
        super(MIREvalMetric, self).__init__(**kwargs)

        self.timestep = kwargs.get("timestep", 0.032)

    def init(self) -> None:
        self.first_batch = None
        self.metrics_names = [
            "Onset_Precision",
            "Onset_Recall",
            "Onset_F-measure",
            "Precision_no_offset",
            "Recall_no_offset",
            "F-measure_no_offset",
            "Precision",
            "Recall",
            "F-measure",
        ]
        self.tmp = torch.zeros(9)
        self.count = 0

    def collect(self, batch_data: dict) -> None:
        assert type(batch_data) == dict
        assert "est" in batch_data
        assert "ref" in batch_data

        raw_est = batch_data["raw_est"]
        est = batch_data["est"]
        ref = batch_data["ref"]
        n = len(est)
        
        data = []

        for i in range(n):
            (est_intervals, est_pitches) = est[i]
            (ref_intervals, ref_pitches) = ref[i]

            if raw_est is None:
                data.append([ref_intervals, ref_pitches, est_intervals, est_pitches])
            else:
                if raw_est[i].shape[0] > 0:
                    argmax = torch.argmax(raw_est[i], dim=-1)
                else:
                    argmax = None
                data.append(
                    [ref_intervals, ref_pitches, est_intervals, est_pitches, argmax]
                )

            if ref_intervals.shape[0] > 0:
                scores = mir_eval.transcription.evaluate(
                    ref_intervals.cpu().numpy(),
                    ref_pitches.cpu().numpy(),
                    est_intervals.cpu().numpy(),
                    est_pitches.cpu().numpy(),
                )
                for j in range(9):
                    name = self.metrics_names[j]
                    self.tmp[j] += scores[name]

                self.count += 1

        if self.first_batch is None:
            self.first_batch = data

    def calculate(self) -> None:
        self.tmp /= self.count
        self.append(self.tmp)

    def summarize(self) -> str:
        metrics = self.get_last().view(9, -1)
        return " ".join(
            [
                f"On: {metrics[2].item()*100:.2f}%",
                f"On&P: {metrics[5].item()*100:.2f}%",
                f"On&P&Off: {metrics[8].item()*100:.2f}%",
            ]
        )

    def benchmark(self) -> dict:
        metrics = self.get_last().view(9, -1)
        res = {name: metrics[i] for i, name in enumerate(self.metrics_names)}
        return res

    def add_tensorboard(
        self, writer: SummaryWriter, step: int = None, tag_prefix: str = ""
    ) -> None:
        import matplotlib.pyplot as plt

        metrics = self.get_last().view(9, -1)
        for i, name in enumerate(self.metrics_names):
            writer.add_scalar(self.name + "_" + name, metrics[i], global_step=step)

        for i, data in enumerate(self.first_batch):
            plt.subplot(211)
            plot_mireval(plt.gca(), data[0], data[1])
            plt.subplot(212)
            plot_mireval(plt.gca(), data[2], data[3])

            writer.add_figure(self.name + str(i), plt.gcf(), global_step=step)

            if len(data) >= 5 and (data[4] is not None):
                seq = []
                for w in data[4]:
                    w = w.item()
                    if w < 128:
                        seq.append(f"NOTE_ON<{w:d}>")
                    elif w < 256:
                        seq.append(f"NOTE_OFF<{w-128:d}>")
                    elif w < 356:
                        seq.append(f"TIME_SHIFT<{w-256:d}>")
                    elif w < 388:
                        seq.append(f"VELOCITY<{w-356:d}>")
                    else:
                        raise Exception(f"error, token {w} unknown")
                writer.add_text(
                    self.name + "_token_" + str(i), ", ".join(seq), global_step=step
                )
