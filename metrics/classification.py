import torch
from torch.utils.tensorboard import SummaryWriter

from torchtrain import Metric

import os


class ClassificationMetric(Metric):
    __name__ = "classification"

    @property
    def name(self):
        prefix = self.tag + "_" if self.tag is not None else ""
        return prefix + self.__name__

    def __init__(self, **kwargs):
        super(ClassificationMetric, self).__init__(**kwargs)

        self.num_class = kwargs.get("num_class", 1)
        self.tag = kwargs.get("tag", None)

    def init(self) -> None:
        self.tp = torch.zeros(self.num_class, dtype=int, device=self.device)
        self.tn = torch.zeros(self.num_class, dtype=int, device=self.device)
        self.fp = torch.zeros(self.num_class, dtype=int, device=self.device)
        self.fn = torch.zeros(self.num_class, dtype=int, device=self.device)
        self.freq = torch.zeros(self.num_class, dtype=int, device=self.device)

    def collect(self, batch_data: dict) -> None:
        if self.tag is not None:
            batch_data = batch_data[self.tag]

        assert type(batch_data) == dict
        assert "prediction" in batch_data and "ground_truth" in batch_data

        pred = batch_data["prediction"] >= 0.5
        gt = batch_data["ground_truth"].bool()

        self.tp += (pred & gt).sum(dim=(0, 1))
        self.tn += ((~pred) & (~gt)).sum(dim=(0, 1))
        self.fp += (pred & (~gt)).sum(dim=(0, 1))
        self.fn += ((~pred) & gt).sum(dim=(0, 1))
        self.freq += gt.sum(dim=(0, 1))

    def calculate(self) -> None:
        accuracy = (self.tp + self.tn).float() / (
            self.tp + self.tn + self.fp + self.fn
        ).float()
        precision = self.tp.float() / (self.tp + self.fp).float()
        recall = self.tp.float() / (self.tp + self.fn).float()
        f1 = 2 * (precision * recall) / (precision + recall)

        accuracy[accuracy != accuracy] = 0
        precision[precision != precision] = 0
        recall[recall != recall] = 0
        f1[f1 != f1] = 0

        self.append(torch.cat([self.freq.float(), accuracy, precision, recall, f1]))

    def summarize(self) -> str:
        prefix = self.tag + "_" if self.tag is not None else ""
        [freq, acc, pre, rec, f1] = self.get_last().view(5, -1)
        return " ".join(
            [
                f"{prefix}imb: 1:{freq.max().float()/freq[freq!=0].min().float():.1f}",
                f"{prefix}acc: {acc.mean()*100:.2f}±{acc.std()*100:.2f}%",
                f"{prefix}pre: {pre.mean()*100:.2f}±{pre.std()*100:.2f}%",
                f"{prefix}rec: {rec.mean()*100:.2f}±{rec.std()*100:.2f}%",
                f"{prefix}f1: {f1.mean()*100:.2f}±{f1.std()*100:.2f}%",
            ]
        )

    def benchmark(self) -> dict:
        prefix = self.tag + "/" if self.tag is not None else ""
        [_, acc, pre, rec, f1] = self.get_last().view(5, -1)
        return {
            f"{prefix}accuracy/mean": acc.mean(),
            f"{prefix}accuracy/std": acc.std(),
            f"{prefix}precision/mean": pre.mean(),
            f"{prefix}precision/std": pre.std(),
            f"{prefix}recall/mean": rec.mean(),
            f"{prefix}recall/std": rec.std(),
            f"{prefix}f1/mean": f1.mean(),
            f"{prefix}f1/std": f1.std(),
        }

    def add_tensorboard(
        self, writer: SummaryWriter, step: int = None, tag_prefix: str = ""
    ) -> None:
        from mpl_toolkits.axes_grid1 import host_subplot
        from matplotlib.ticker import FuncFormatter
        import mpl_toolkits.axisartist as AA
        import matplotlib.pyplot as plt

        [freq, acc, pre, rec, f1] = self.get_last().view(5, -1)

        freq = freq.clone().detach().cpu()
        acc = acc.clone().detach().cpu()
        pre = pre.clone().detach().cpu()
        rec = rec.clone().detach().cpu()
        f1 = f1.clone().detach().cpu()
        index = torch.arange(0, self.num_class, dtype=int)

        fig = plt.gcf()
        ax = host_subplot(111, axes_class=AA.Axes)
        ax_alt = ax.twinx()

        def notes(x, pos):
            notes_list = [
                "C",
                "C#",
                "D",
                "D#",
                "E",
                "F",
                "F#",
                "G",
                "G#",
                "A",
                "A#",
                "B",
            ]
            return notes_list[int(x) % 12] + str((int(x) // 12) - 1)

        formatterx = FuncFormatter(notes)
        ax.xaxis.set_major_formatter(formatterx)

        def percentage(x, pos):
            return "{:.1f}%".format(x * 100)

        formattery = FuncFormatter(percentage)
        ax.yaxis.set_major_formatter(formattery)

        ax_alt.plot(index, freq.numpy(), label="frequency")
        ax_alt.set_yscale("log")

        ax.scatter(index, acc.numpy(), label="accuracy")
        ax.scatter(index, pre.numpy(), label="precision")
        ax.scatter(index, rec.numpy(), label="recall")
        ax.scatter(index, f1.numpy(), label="f1")

        ax.set_xlabel("classes")
        ax.set_ylabel("pourcentage")
        ax_alt.set_ylabel("frequency of occurrence")
        ax.legend()
        fig.tight_layout()

        if self.tag is not None:
            writer.add_figure(
                os.path.join(tag_prefix, self.tag, self.name), fig, global_step=step
            )
        else:
            writer.add_figure(
                os.path.join(tag_prefix, self.name), fig, global_step=step
            )
