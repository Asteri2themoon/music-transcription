import torch
from torch.utils.tensorboard import SummaryWriter
import numpy as np
import mir_eval

from torchtrain import Metric

import time
import json
import os

import sys

sys.path.append("..")
from utils import plot_mireval


class TopKMetric(Metric):
    __name__ = "metrics.top_k"

    def __init__(self, **kwargs):
        super(TopKMetric, self).__init__(**kwargs)

    def init(self) -> None:
        self.tmp = torch.zeros(2)
        self.count = 0

    def collect(self, batch_data: dict) -> None:
        assert type(batch_data) == dict
        assert "raw_est" in batch_data
        assert "seq" in batch_data

        raw_est = batch_data["raw_est"]
        ref = batch_data["seq"]
        top1, top5, count = 0, 0, 0
        for r, e in zip(ref, raw_est):
            if r.shape[0] != 0:
                topk = torch.topk(e, k=5, dim=-1).indices.t()
                correct = topk.eq(r.expand_as(topk))
                top1 += correct[:1].view(-1).float().sum() / r.shape[0]
                top5 += correct[:5].view(-1).float().sum() / r.shape[0]
                count += 1
        self.tmp[0] += top1 / count
        self.tmp[1] += top5 / count
        self.count += 1

    def calculate(self) -> None:
        self.tmp /= self.count
        self.append(self.tmp)

    def summarize(self) -> str:
        metrics = self.get_last().view(2, -1)
        return " ".join(
            [
                f"top-1: {metrics[0].item()*100:.2f}%",
                f"top-5: {metrics[1].item()*100:.2f}%",
            ]
        )

    def benchmark(self) -> dict:
        metrics = self.get_last().view(2, -1)
        res = {"top-1": metrics[0], "top-5": metrics[1]}
        return res

    def add_tensorboard(
        self, writer: SummaryWriter, step: int = None, tag_prefix: str = ""
    ) -> None:
        import matplotlib.pyplot as plt

        metrics = self.get_last().view(2, -1)
        writer.add_scalar(self.name + "_top1", metrics[0], global_step=step)
        writer.add_scalar(self.name + "_top5", metrics[1], global_step=step)
