import torch
from torch.utils.tensorboard import SummaryWriter

from torchtrain import Metric

import os


class GlobalClassificationMetric(Metric):
    __name__ = "globalclassification"

    def __init__(self, **kwargs):
        super(GlobalClassificationMetric, self).__init__(**kwargs)

    def init(self) -> None:
        self.tp = torch.zeros(1, dtype=int, device=self.device)
        self.tn = torch.zeros(1, dtype=int, device=self.device)
        self.fp = torch.zeros(1, dtype=int, device=self.device)
        self.fn = torch.zeros(1, dtype=int, device=self.device)

    def collect(self, batch_data: dict) -> None:
        assert type(batch_data) == dict
        assert "prediction" in batch_data and "ground_truth" in batch_data

        pred = batch_data["prediction"] >= 0.5
        gt = batch_data["ground_truth"].bool()

        self.tp += (pred & gt).sum()
        self.tn += ((~pred) & (~gt)).sum()
        self.fp += (pred & (~gt)).sum()
        self.fn += ((~pred) & gt).sum()

    def calculate(self) -> None:
        accuracy = (self.tp + self.tn).float() / (
            self.tp + self.tn + self.fp + self.fn
        ).float()
        precision = self.tp.float() / (self.tp + self.fp).float()
        recall = self.tp.float() / (self.tp + self.fn).float()
        f1 = 2 * (precision * recall) / (precision + recall)

        accuracy[accuracy != accuracy] = 0
        precision[precision != precision] = 0
        recall[recall != recall] = 0
        f1[f1 != f1] = 0

        self.append(torch.cat([accuracy, precision, recall, f1]))

    def summarize(self) -> str:
        [acc, pre, rec, f1] = self.get_last().view(4, -1)
        return " ".join(
            [
                f"g_pre: {pre.item()*100:.2f}%",
                f"g_rec: {rec.item()*100:.2f}%",
                f"g_f1: {f1.item()*100:.2f}%",
            ]
        )

    def benchmark(self) -> dict:
        [acc, pre, rec, f1] = self.get_last().view(4, -1)
        return {
            "accuracy": acc.item(),
            "precision": pre.item(),
            "recall": rec.item(),
            "f1": f1.item(),
        }

    def add_tensorboard(
        self, writer: SummaryWriter, step: int = None, tag_prefix: str = ""
    ) -> None:
        [acc, pre, rec, f1] = self.get_last().view(4, -1)
        writer.add_scalar(
            os.path.join(tag_prefix, self.name, "accuracy"),
            acc.item(),
            global_step=step,
        )
        writer.add_scalar(
            os.path.join(tag_prefix, self.name, "precision"),
            pre.item(),
            global_step=step,
        )
        writer.add_scalar(
            os.path.join(tag_prefix, self.name, "recall"), rec.item(), global_step=step
        )
        writer.add_scalar(
            os.path.join(tag_prefix, self.name, "f1"), f1.item(), global_step=step
        )
