import torch
from torch.utils.tensorboard import SummaryWriter

import matplotlib.pyplot as plt

from torchtrain import Metric

import os


class MAPMetric(Metric):
    __name__ = "mAP"

    def __init__(self, **kwargs):
        super(MAPMetric, self).__init__(**kwargs)

        self.n = kwargs.get("n", 101)
        self.num_class = kwargs.get("num_class", 1)

    def init(self) -> None:
        self.tp = torch.zeros((self.num_class, self.n), dtype=int, device=self.device)
        self.tn = torch.zeros_like(self.tp)
        self.fp = torch.zeros_like(self.tp)
        self.fn = torch.zeros_like(self.tp)

    def collect(self, batch_data: dict) -> None:
        assert type(batch_data) == dict
        assert "prediction" in batch_data and "ground_truth" in batch_data

        gt = batch_data["ground_truth"].bool()
        gt = gt.view(*gt.shape, 1).repeat(1, 1, 1, self.n)

        k = torch.linspace(0.0, 1.0, steps=self.n, device=self.device)
        c, i_k = torch.meshgrid(
            torch.arange(0, self.num_class, 1, dtype=int, device=self.device),
            torch.arange(0, self.n, 1, dtype=int, device=self.device),
        )
        pred = batch_data["prediction"][:, :, c] >= k[i_k]

        self.tp += (pred & gt).sum(dim=(0, 1))
        self.fp += (pred & (~gt)).sum(dim=(0, 1))
        self.fn += ((~pred) & gt).sum(dim=(0, 1))

    def calculate(self) -> None:
        self.precision = self.tp.float() / (self.tp + self.fp).float()
        self.recall = self.tp.float() / (self.tp + self.fn).float()

        self.precision[self.precision != self.precision] = 0
        self.recall[self.recall != self.recall] = 0

        P = 0.5 * (self.precision[:, 1:] + self.precision[:, :-1])
        recall_step = self.recall[:, :-1] - self.recall[:, 1:]
        if (recall_step < 0.0).sum() != 0:
            print("WARNING! strange behaviour in mAP metric")

        AP = (P * recall_step).sum(dim=1)

        mAP = AP.mean()

        self.precision = self.precision.mean(dim=0)
        self.recall = self.recall.mean(dim=0)

        self.append(mAP)

    def summarize(self) -> str:
        return f"mAP {self.get_last().item()*100:.2f}%"

    def benchmark(self) -> dict:
        return {
            "mAP": self.get_last().item(),
        }

    def add_tensorboard(
        self, writer: SummaryWriter, step: int = None, tag_prefix: str = ""
    ) -> None:
        writer.add_scalar(
            os.path.join(tag_prefix, self.name),
            self.get_last().item(),
            global_step=step,
        )

        figure = plt.figure()
        plt.plot(self.recall.cpu(), self.precision.cpu())
        plt.title("Precision vs recall")
        plt.xlabel("recall")
        plt.ylabel("precision")
        plt.xlim((0.0, 1.0))
        plt.ylim((0.0, 1.0))
        writer.add_figure(
            os.path.join(tag_prefix, self.name, "precision_vs_recall"),
            figure,
            global_step=step,
        )

