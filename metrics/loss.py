import torch
from torch.utils.tensorboard import SummaryWriter

from torchtrain import Metric

from typing import Any
import os


class LossMetric(Metric):
    __name__ = "loss"

    def __init__(self, **kwargs):
        super(LossMetric, self).__init__(**kwargs)

    def init(self) -> None:
        self.loss_count = 0
        self.loss_sum = 0
        self.last_loss = 0

    def collect(self, batch_data: dict) -> None:
        assert "loss" in batch_data

        self.last_loss = batch_data["loss"]
        self.loss_count += 1
        self.loss_sum += self.last_loss

    def calculate(self) -> None:
        self.append(torch.tensor([self.loss_sum / self.loss_count]))

    def summarize(self) -> str:
        return f"loss: {self.get_last()[0].item():.5f}"

    def benchmark(self) -> Any:
        return False

    def add_tensorboard(
        self, writer: SummaryWriter, step: int = None, tag_prefix: str = ""
    ) -> None:
        writer.add_scalar(
            os.path.join(tag_prefix, self.name), self.metrics[-1][0], global_step=step
        )

    def preview(self) -> Any:
        return f"loss: {self.last_loss.item():.5f}"
